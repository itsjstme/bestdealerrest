<?php
/**
 * Database configuration
 */

 // http://bestdealer.waayout.com/

	define('DB_USERNAME', 'itsjstme' );
	define('DB_PASSWORD', $_ENV["DB_PASSWORD"] );
	define('DB_HOST',  'localhost' );
	define('DB_NAME', 'bestdealer' );


	define('PURCHASED'		, 0);
	define('SOLD'			, 1);
	define('DELETED'		, 2);	
	define('PREPPED'		, 3);	
	
	define('ARCHIVE'		, -1);
	

	define( 'XML_GEO' , dirname(__FILE__) . '/../data/zip_geo.xml' );
	define( 'XML_CRAIGSLIST' , dirname(__FILE__) . '/../data/zip_to_craigslist.xml' );

	define( 'EDMUNDS_API' , 'rpwhm99xyh8wqnzm2hebqse9' );
	define( 'STYLE_FOLDER' , dirname(__FILE__) .'/../data/style/' );
	define( 'VIN_FOLDER' , dirname(__FILE__) .'/../data/vin/' );
	define( 'DATA_FOLDER' , dirname(__FILE__) .'/../data/' );
	define( 'DATA_JSON_FOLDER' , dirname(__FILE__).'/../data/json' );
	
	define( 'EBAY_DEVID' , '4b291fa2-0f83-42e8-9e5e-fb1388122141' );
	define( 'EBAY_APPID' , 'WaayOutB-a297-4fee-985c-d13b5e2de13a'  );
	define( 'EBAY_CERTID' , 'ccd3b505-42fb-4559-859b-146403634a2c' );

	define( 'PROD_STRIPE_API_KEY' , 'sk_test_UgubuyDgnpNnbuvzMUCRgU92' );
	define( 'DEV_STRIPE_API_KEY' , 'sk_live_KavdgnYSCUiNjlLAaLRwSDJ3'  );
	

	$ebayTEST = array ( 'DEVID' => '4b291fa2-0f83-42e8-9e5e-fb1388122141' , 'AppID' => 'WaayOutB-4768-469c-b7ed-ac53823feb42' , 'CertID' => '73c71671-ed45-42fe-af23-f6a86ab597d2' );
	$ebayPROD = array ( 'DEVID' => '4b291fa2-0f83-42e8-9e5e-fb1388122141' , 'AppID' => 'WaayOutB-4768-469c-b7ed-ac53823feb42' , 'CertID' => '73c71671-ed45-42fe-af23-f6a86ab597d2' );


	function fix_latin1_mangled_with_utf8_maybe_hopefully_most_of_the_time($str){
		return preg_replace_callback('#[\\xA1-\\xFF](?![\\x80-\\xBF]{2,})#', 'utf8_encode_callback', $str);
	}

	function utf8_encode_callback($m){
		return utf8_encode($m[0]);
	}

	function findInfo($zipCode){

		if (file_exists(XML_GEO)) {			
			$xml = simplexml_load_file(XML_GEO);				
			$zip = $xml->xpath('/ROWSET/ROW[zip="'.$zipCode.'"]');			
			if(isset($zip) && !empty($zip))
				return $zip[0];				 
		} 
		
		return false;
	
	}
	
	function findCraigslist($zipCode){
	
		if (file_exists(XML_CRAIGSLIST)) {
			
			$xml = simplexml_load_file(XML_CRAIGSLIST);			
			$zip = $xml->xpath('/ROWSET/ROW[zip="'.$zipCode.'"]');
			
			if(isset($zip[0]))
				return $zip[0];
		 
		} 
			
		return false;
	
	}		

	function GetVehicleResults($fileName){
		
		$contents = file_get_contents($fileName);
		if(!empty($contents)){
			return $contents;
		}
	
		return false;
		
	}		
	
	function getConditionalProbabilty($higher, $lower, $Data) {
	  $NumAB   	= 0;
	  $NumB    	= 0;
	  
	 $NumB 		= count(arrayContainsValueOverLimit($Data, $lower));
	 $NumAB 	= count(arrayContainsValueOverLimit($Data, $higher));

	  return $NumAB / $NumB;
	}



	function arrayContainsValueOverLimit($arr, $limit) {

			$newNumbers = array_filter(
				$arr ,
				function ($value) use($limit) {
					return ($value >= $limit );
				}
			);

			return $newNumbers;			
	
	}
	
	function arrayContainsValueLowerLimit($arr, $limit) {
			
			$newNumbers = array_filter(
				$arr ,
				function ($value) use($limit) {
					return ($value <= $limit );
				}
			);
			
			return $newNumbers;
	}	
	
?>
