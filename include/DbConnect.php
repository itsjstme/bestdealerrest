<?php

/**
 * Handling database connection
 *
 * @author Ravi Tamada
 * @link URL Tutorial link
 */
class DbConnect {

    private $conn;

    function __construct() {        
        
        $this->connect();
        
    }
        

    /**
     * Establishing database connection
     * @return database connection handler
     */
    function connect() {
        include_once dirname(__FILE__) . '/Config.php';

		$dsn = 'mysql:dbname='.DB_NAME.';host='.DB_HOST;
        // Connecting to mysql database
        // $this->conn = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
		$this->conn = new PDO($dsn, DB_USERNAME , DB_PASSWORD );

        // Check for database connection error
        if (mysqli_connect_errno()) {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }

		$this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        // returing connection resource
        return $this->conn;
    }
	
    function getConnection() {
        
		if($this->conn)
			return $this->conn;
		else{
			return $this->connect();
		}		
    }	


    function insert($params , $tableName){

			$response = array();
			// First check if user already existed in db               
            $params["created"]   = date("Y-m-d H:i:s");                 
            // insert query
			
			$sql = "INSERT INTO `{$tableName}` (".implode("," , array_keys($params)).") VALUES (".implode( "," , array_map(function($value) { return ':' . $value; }, array_keys($params) ) ).")";
			// insert query
			$stmt = $this->conn->prepare($sql);
			
			foreach($params as $key => $value ){
				$stmt->bindValue($key, $value); //$stmt->bindParam(":".$key , $value );
			}
			
			return  $stmt->execute();		
			
    }
    
    function update($params , $tableName , $condition = null  ){

            // insert query
            //$stmt = $this->conn->prepare("UPDATE users ( email, password , api_key, status) values(  :email , :password , :api_key , 1) WHERE userid = :userID ");
            $sql = " UPDATE `{$tableName}` SET " . implode( " , " , array_map(function($value) { return  $value.' = :' .$value; }, array_keys($params) ) ) . "  {$condition} " ; 
            // $stmt->bindParam(":name", $name);
            $stmt = $this->conn->prepare($sql);
            
            foreach($params as $key => $value ){
                $stmt->bindValue($key, $value); //$stmt->bindParam(":".$key , $value );
            }
            // $stmt->bindParam(":vin", $vinNumber);
            return $stmt->execute(); 
			
			
    }    

}

?>
