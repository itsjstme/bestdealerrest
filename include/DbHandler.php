<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Ravi Tamada
 * @link URL Tutorial link
 */
class DbHandler {

    private $conn;

    function __construct() {
        require_once dirname(__FILE__) . '/DbConnect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }


    public function beginTransaction()
    {
        $this->conn->beginTransaction();
    }

    public function commit()
    {
        $this->conn->commit();
    }

    public function rollback()
    {
        $this->conn->rollback();
    }

    /**
     * Creating new user
     * @param String $name User full name
     * @param String $email User login email userid
     * @param String $password User login password
     */
    private function updateUserProfile($params , $userID) {
        
        $response = array();

        // First check if user already existed in db
        if ($this->isUserExists($params["email"])) {
            // Generating password hash
            $password = PassHash::hash($params["password"]);

            // Generating API key
            $params["api_key"] = $this->generateApiKey();

            // insert query
            //$stmt = $this->conn->prepare("UPDATE users ( email, password , api_key, status) values(  :email , :password , :api_key , 1) WHERE userid = :userID ");
            $sql = " UPDATE users SET " . implode( " , " , array_map(function($value) { return  $value.' = :' .$value; }, array_keys($params) ) ) . " WHERE email = '{$userID}' " ; 
            // $stmt->bindParam(":name", $name);
            $stmt = $this->conn->prepare($sql);
            
            foreach($params as $key => $value ){
                $stmt->bindValue($key, $value); //$stmt->bindParam(":".$key , $value );
            }
            // $stmt->bindParam(":vin", $vinNumber);
            $result = $stmt->execute(); 

            // Check for successful insertion
            if ($result) {
                // User successfully inserted
                return USER_CREATED_SUCCESSFULLY;
            } else {
                // Failed to create user
                return USER_CREATE_FAILED;
            }
        } else {
            // User with same email already existed in the db
            return USER_ALREADY_EXISTED;
        }

        return $response;
    }

    public function lastVisited($userID){

        $stmt = $this->conn->prepare("UPDATE users set lastVisited = :lastVisited WHERE userid = :userID ");
        // $stmt->bindParam(":name", $name);
        $stmt->bindParam(":lastVisited", date("Y-m-d H:i:s") );
        $stmt->bindParam(":userID", $userID );
        return $stmt->execute();

    }

    public function deactivateUser($userID){

        $stmt = $this->conn->prepare("UPDATE users set active = 0 WHERE userid = :userID ");
        // $stmt->bindParam(":name", $name);
        $stmt->bindParam(":userID", $userID );
        return $stmt->execute();

    }
    
    public function addPayment($userID){

        $stmt = $this->conn->prepare("UPDATE users set active = 0 WHERE userid = :userID ");
        // $stmt->bindParam(":name", $name);
        $stmt->bindParam(":userID", $userID );
        return $stmt->execute();

    }
    
    public function resetUser($email){
        
        require_once 'PassHash.php';
        $generatePassword = generateRandomString();
        $password = PassHash::hash($password);
        $stmt = $this->conn->prepare("UPDATE users set active = 0 WHERE email = :userID ");
        // $stmt->bindParam(":name", $name);
        $stmt->bindParam(":userID", $userID );
        return $stmt->execute();

    }    
    
    


    /* ------------- `users` table method ------------------ */

    /**
     * Creating new user
     * @param String $name User full name
     * @param String $email User login email userid
     * @param String $password User login password
     */
    public function createUser($params) {

        
        require_once 'PassHash.php';
        $response = array();

		    extract($params, EXTR_PREFIX_SAME, "wddx");
		
        // First check if user already existed in db
        if (!$this->isUserExists($email)) {
            // Generating password hash
            $password = PassHash::hash($password);

            // Generating API key
            $api_key = $this->generateApiKey();
			
			$params["password"]  = $password;
			$params["api_key"]   = $api_key;
            $params["status"]    = 1;
            $params["ipaddress"] = getIPAddress();
            $params["created"]   = date("Y-m-d H:i:s");     
            
            $sql = "INSERT INTO users (".implode("," , array_keys($params)).") VALUES (".implode( "," , array_map(function($value) { return ':' . $value; }, array_keys($params) ) ).")";
            // insert query
            $stmt = $this->conn->prepare($sql);
			
            foreach($params as $key => $value ){
                $stmt->bindValue($key, $value); //$stmt->bindParam(":".$key , $value );
            }			
            


            $result = $stmt->execute();            

            // Check for successful insertion
            if ($result) {
                // User successfully inserted
                return USER_CREATED_SUCCESSFULLY;
            } else {
                // Failed to create user
                return USER_CREATE_FAILED;
            }
        } else {
            // User with same email already existed in the db
            return USER_ALREADY_EXISTED;
        }

        return $response;
    }

    /**
     * Creating new user
     * @param String $name User full name
     * @param String $email User login email userid
     * @param String $password User login password
     */
    public function updateUser($params) {
        
        $response = array();

         extract($params, EXTR_PREFIX_SAME, "wddx");
    
        // First check if user already existed in db
        if ($this->isUserExists($email)) {
            // Generating password hash
            if(isset($password) && !empty($password)){
                require_once 'PassHash.php';
                $password = PassHash::hash($password);
                // Generating API key
                $api_key = $this->generateApiKey();
                $params["password"]  = $password;
            }
			      
            $params["status"]    = 1;
            $params["ipaddress"] = getIPAddress();

            $sql = " UPDATE users SET " . implode( " , " , array_map(function($value) { return  $value.' = :' .$value; }, array_keys($params) ) ) . " WHERE email = '{$email}' " ; 
            // insert query
            $stmt = $this->conn->prepare($sql);
            
            foreach($params as $key => $value ){
                $stmt->bindValue($key, $value); //$stmt->bindParam(":".$key , $value );
            }
            // $stmt->bindParam(":vin", $vinNumber);
            $result = $stmt->execute(); 
          

            // Check for successful insertion
            if ($result) {
                // User successfully inserted
                return USER_CREATED_SUCCESSFULLY;
            } else {
                // Failed to create user
                return USER_CREATE_FAILED;
            }
        } else {
            // User with same email already existed in the db
            return USER_ALREADY_EXISTED;
        }

        return $response;
    }


    /**
     * Checking user login
     * @param String $email User login email userid
     * @param String $password User login password
     * @return boolean User login status success/fail
     */
    public function checkLogin($email, $password) {
        // fetching user by email
        $stmt = $this->conn->prepare("SELECT password , status FROM users WHERE email = :email");
        $stmt->bindParam(":email", $email);
        $stmt->execute();
        $result = $stmt->fetch();
		
        if (isset($result["password"])) {
            // Found user with the email
            // Now verify the password
            if($result["status"])
                return PassHash::check_password($result["password"], $password);
            else
                return NULL;
        } else {           
            // user not existed with the email
            return FALSE;
        }
    }

    /**
     * Checking for duplicate user by email address
     * @param String $email email to check in db
     * @return boolean
     */
    private function isUserExists($email) {
        $stmt = $this->conn->prepare("SELECT userid from users WHERE email = :email");
        $stmt->bindParam(":email", $email);
        $stmt->execute();
        $results = $stmt->fetchAll();
        return (count($results) >= 1);
    }

    /**
     * Fetching user by email
     * @param String $email User email userid
     */
    public function getUserByEmail($email) {
        $stmt = $this->conn->prepare("SELECT  userid , email, api_key, status, created FROM users WHERE email = :email");
        $stmt->bindParam(":email", $email);
        if ($stmt->execute()) {
            // $user = $stmt->get_result()->fetch_assoc();;
            $user = $stmt->fetch();
            return $user;
        } else {
            return NULL;
        }
    }

    /**
     * Fetching user api key
     * @param String $user_id user userid primary key in user table
     */
    public function getApiKeyById($user_id) {
        $stmt = $this->conn->prepare("SELECT api_key FROM users WHERE userid = :userid");
        $stmt->bindParam(":userid", $user_id);
        if ($stmt->execute()) {
            // $api_key = $stmt->get_result()->fetch_assoc();
            // TODO
            $user = $stmt->fetch();         
            return $user["api_key"];
        } else {
            return NULL;
        }
    }

    /**
     * Fetching user userid by api key
     * @param String $api_key user api key
     */
    public function getUserId($api_key) {
        $stmt = $this->conn->prepare("SELECT userid , customerid   FROM users WHERE api_key = :api_key");
        $stmt->bindParam(":api_key", $api_key);
        if ($stmt->execute()) {
            $user = $stmt->fetch();  			
            return $user;
            // TODO
            // $user_id = $stmt->get_result()->fetch_assoc();
            
        } else {
            return NULL;
        }
    }

    /**
     * Validating user api key
     * If the api key is there in db, it is a valid key
     * @param String $api_key user api key
     * @return boolean
     */
    public function isValidApiKey($api_key) {
        $stmt = $this->conn->prepare("SELECT userid  FROM users WHERE api_key = :api_key");
        $stmt->bindParam(":api_key", $api_key);
        if($stmt->execute()){
			return  $stmt->fetch();	
		}
			return false;
    }

    /**
     * Generating random Unique MD5 String for user Api key
     */
    private function generateApiKey() {
        return UUID::v4(); //md5(uniqid(rand(), true));
    }



}



class UUID {

  public static function v3($namespace, $name) {
    if(!self::is_valid($namespace)) return false;

    // Get hexadecimal components of namespace
    $nhex = str_replace(array('-','{','}'), '', $namespace);

    // Binary Value
    $nstr = '';

    // Convert Namespace UUID to bits
    for($i = 0; $i < strlen($nhex); $i+=2) {
      $nstr .= chr(hexdec($nhex[$i].$nhex[$i+1]));
    }

    // Calculate hash value
    $hash = md5($nstr . $name);

    return sprintf('%08s-%04s-%04x-%04x-%12s',

      // 32 bits for "time_low"
      substr($hash, 0, 8),

      // 16 bits for "time_mid"
      substr($hash, 8, 4),

      // 16 bits for "time_hi_and_version",
      // four most significant bits holds version number 3
      (hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x3000,

      // 16 bits, 8 bits for "clk_seq_hi_res",
      // 8 bits for "clk_seq_low",
      // two most significant bits holds zero and one for variant DCE1.1
      (hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,

      // 48 bits for "node"
      substr($hash, 20, 12)
    );
  }

  public static function v4() {
    return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',

      // 32 bits for "time_low"
      mt_rand(0, 0xffff), mt_rand(0, 0xffff),

      // 16 bits for "time_mid"
      mt_rand(0, 0xffff),

      // 16 bits for "time_hi_and_version",
      // four most significant bits holds version number 4
      mt_rand(0, 0x0fff) | 0x4000,

      // 16 bits, 8 bits for "clk_seq_hi_res",
      // 8 bits for "clk_seq_low",
      // two most significant bits holds zero and one for variant DCE1.1
      mt_rand(0, 0x3fff) | 0x8000,

      // 48 bits for "node"
      mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
    );
  }

  public static function v5($namespace, $name) {
    if(!self::is_valid($namespace)) return false;

    // Get hexadecimal components of namespace
    $nhex = str_replace(array('-','{','}'), '', $namespace);

    // Binary Value
    $nstr = '';

    // Convert Namespace UUID to bits
    for($i = 0; $i < strlen($nhex); $i+=2) {
      $nstr .= chr(hexdec($nhex[$i].$nhex[$i+1]));
    }

    // Calculate hash value
    $hash = sha1($nstr . $name);

    return sprintf('%08s-%04s-%04x-%04x-%12s',

      // 32 bits for "time_low"
      substr($hash, 0, 8),

      // 16 bits for "time_mid"
      substr($hash, 8, 4),

      // 16 bits for "time_hi_and_version",
      // four most significant bits holds version number 5
      (hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x5000,

      // 16 bits, 8 bits for "clk_seq_hi_res",
      // 8 bits for "clk_seq_low",
      // two most significant bits holds zero and one for variant DCE1.1
      (hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,

      // 48 bits for "node"
      substr($hash, 20, 12)
    );
  }

  public static function is_valid($uuid) {
    return preg_match('/^\{?[0-9a-f]{8}\-?[0-9a-f]{4}\-?[0-9a-f]{4}\-?'.'[0-9a-f]{4}\-?[0-9a-f]{12}\}?$/i', $uuid) === 1;
  }
}


?>
