<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Ravi Tamada
 * @link URL Tutorial link
 */
class Users {

    private $conn;

    function __construct() {
        
        $results["status"] = false;
        $results["error"]  = "";

        require_once dirname(__FILE__) . '/Config.php';

        try {
            $dsn = 'mysql:dbname='+DB_NAME+';host=' + DB_HOST + ';port='+ DB_PORT;
            $this->conn = new PDO($dsn, DB_USERNAME  , DB_PASSWORD );
            $results["status"] =  true;
        } catch (PDOException $e) {
            $results["error"]  = 'Connection failed: ' . $e->getMessage();
        }

        return $results;

    }

    /* ------------- `users` table method ------------------ */

    /**
     * Creating new user
     * @param String $name User full name
     * @param String $email User login email id
     * @param String $password User login password
     */
    public function createUser($name, $email, $password) {
        require_once 'PassHash.php';
        $response = array();


        $sth = $this->dbh->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));

        // First check if user already existed in db
        if (!$this->isUserExists($email)) {
            // Generating password hash
            $password_hash = PassHash::hash($password);

            // Generating API key
            $api_key = $this->generateApiKey();

            // insert query
            $stmt = $this->conn->prepare("INSERT INTO users(name, email, password_hash, api_key, status) values(?, ?, ?, ?, 1)");
            $stmt->bind_param("ssss", $name, $email, $password_hash, $api_key);

            $result = $stmt->execute();

            

            // Check for successful insertion
            if ($result) {
                // User successfully inserted
                return USER_CREATED_SUCCESSFULLY;
            } else {
                // Failed to create user
                return USER_CREATE_FAILED;
            }
        } else {
            // User with same email already existed in the db
            return USER_ALREADY_EXISTED;
        }

        return $response;
    }

    /**
     * Checking user login
     * @param String $email User login email id
     * @param String $password User login password
     * @return boolean User login status success/fail
     */
    public function checkLogin($email, $password , $socialLogin = false) {
        

        $sql = 'SELECT userId , status FROM users WHERE  email = :email';
        
        $sth = $this->dbh->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute( array(':email' => $email ));

        $result = $sth->fetch(PDO::FETCH_ASSOC);

        return $result;

        // fetching user by email
        $stmt = $this->conn->prepare("SELECT password_hash FROM users WHERE email = ?");

        $stmt->bind_param("s", $email);

        $stmt->execute();

        $stmt->bindParam($password_hash);

        $stmt->store_result();

        if ($stmt->num_rows > 0) {
            // Found user with the email
            // Now verify the password

            $stmt->fetchAll();

            

            if (PassHash::check_password($password_hash, $password)) {
                // User password is correct
                return TRUE;
            } else {
                // user password is incorrect
                return FALSE;
            }
        } else {
            

            // user not existed with the email
            return FALSE;
        }
    }

    /**
     * Checking for duplicate user by email address
     * @param String $email email to check in db
     * @return boolean
     */
    private function isUserExists($email) {

        $sql = 'SELECT userId , status FROM users WHERE  email = :email';
        
        $sth = $this->dbh->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute( array(':email' => $email ));

        $result = $sth->fetch(PDO::FETCH_ASSOC);

        return $result;
    }

    /**
     * Fetching user by email
     * @param String $email User email id
     */
    public function getUserByEmail($email) {

        $sql = 'SELECT name, email, api_key, status, created_at FROM users WHERE email = :email';
        
        $sth = $this->dbh->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute( array(':email' => $email ));

        $result = $sth->fetch(PDO::FETCH_ASSOC);

        if (is_array($result)) {
            return $result;
        } else {
            return NULL;
        }
    }

    /**
     * Fetching user api key
     * @param String $user_id user id primary key in user table
     */
    public function getApiKeyById($user_id) {

        $sql = 'SELECT api_key FROM users WHERE id = :userId';        
        $sth = $this->dbh->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute( array(':userId' => $email ));        
        
        $result = $sth->fetch(PDO::FETCH_ASSOC);

        if (is_array($result)) {
            return $result;
        } else {
            return NULL;
        }

    }

    /**
     * Fetching user id by api key
     * @param String $api_key user api key
     */
    public function  getUserIDAPIKey($api_key) {

        $sql = 'SELECT id FROM users WHERE api_key =:api_key';        
        $sth = $this->dbh->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute( array(':api_key' => $api_key ));        
        
        $result = $sth->fetch(PDO::FETCH_ASSOC);

        if (is_array($result)) {
            return $result;
        } else {
            return NULL;
        }

    }

    /**
     * Validating user api key
     * If the api key is there in db, it is a valid key
     * @param String $api_key user api key
     * @return boolean
     */
    public function IsValidAPIKey($api_key) {

        $sql = 'SELECT id from users WHERE api_key = :api_key';        
        $sth = $this->dbh->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute( array(':api_key' => $api_key ));        
        
        $result = $sth->fetch(PDO::FETCH_ASSOC);

        if (is_array($result)) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * Generating random Unique MD5 String for user Api key
     */
    private function GenerateAPIKey() {
        return md5(uniqid(rand(), true));
    }

 

}

?>
