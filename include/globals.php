<?php

require_once 'validators.php';

function array2xml($array, $xml = false){
    if($xml === false){
        $xml = new SimpleXMLElement('<root/>');
    }
    foreach($array as $key => $value){
        if(is_array($value)){
            array2xml($value, $xml->addChild($key));
        }else{
            $xml->addChild($key, $value);
        }
    }
    return $xml->asXML();
}



/**
 * Adding Middle Layer to authenticate every request
 * Checking if the request has valid api key in the 'Authorization' header
 */
function authenticate(\Slim\Route $route) {
    // Getting request headers
    $headers = apache_request_headers();
    $response = array();
    $app = \Slim\Slim::getInstance();
    // default
    $response["error"] = true;
    $response["message"] = "Api key is misssing";

    // Verifying Authorization Header
    if (isset($headers['Authorization'])) {
        $db = new DbHandler();

        // get the api key
        $api_key = $headers['Authorization'];
        $response = $db->isValidApiKey($api_key);
        // validating api key
        if (!$response["status"]) {
            // api key is not present in users table
            echoRespnse(401, $response);
            $app->stop();
        } else {
            global $user_id;
            // get user primary key id
            $user_id = $response["id"];
        }
    } else {
        // api key is missing in header
        echoRespnse(400, $response);
        $app->stop();
    }
}





/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
    $error          = false;
    $error_fields   = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }

    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(400, $response);
        $app->stop();
    }
}


/**
 * Echoing json or xml response to client
 * @param String $status_code Http response code
 * @param Int $response Json/XML response
 */
function echoRespnse($status_code, $response ,$isXML = false) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);

    // setting response content type to json
    if($isXML){
        
        $app->contentType('application/xml');
        $xml = new SimpleXMLElement('<root/>');
        array_walk_recursive($response, array ($xml, 'addChild'));  
        echo $xml->asXML();      
    }    
    else{    
        
        $app->contentType('application/json');
        echo json_encode($response); 

     }   
       

    
}
      


?>