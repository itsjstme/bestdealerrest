<?php

/* -------   Validators ------------------------------ */


function validate_vin_number($vin) {

    $vin = strtolower($vin);
    if (!preg_match('/^[^\Wioq]{17}$/', $vin)) { 
        return false; 
    }

    $weights = array(8, 7, 6, 5, 4, 3, 2, 10, 0, 9, 8, 7, 6, 5, 4, 3, 2);

    $transliterations = array(
        "a" => 1, "b" => 2, "c" => 3, "d" => 4,
        "e" => 5, "f" => 6, "g" => 7, "h" => 8,
        "j" => 1, "k" => 2, "l" => 3, "m" => 4,
        "n" => 5, "p" => 7, "r" => 9, "s" => 2,
        "t" => 3, "u" => 4, "v" => 5, "w" => 6,
        "x" => 7, "y" => 8, "z" => 9
    );

    $sum = 0;

    for($i = 0 ; $i < strlen($vin) ; $i++ ) { // loop through characters of VIN
        // add transliterations * weight of their positions to get the sum
        if(!is_numeric($vin{$i})) {
            $sum += $transliterations[$vin{$i}] * $weights[$i];
        } else {
            $sum += $vin{$i} * $weights[$i];
        }
    }

    // find checkdigit by taking the mod of the sum

    $checkdigit = $sum % 11;

    if($checkdigit == 10) { // checkdigit of 10 is represented by "X"
        $checkdigit = "x";
    }

    return ($checkdigit == $vin{8});
}

function validate_vin($vin){

	$returnValue = false;

	if(strlen($str) != 17){
		$returnValue = preg_match('#^([A-HJ-NPR-Z]{3}|\d{3})[A-HJ-NPR-Z]{2}\d{2}([A-HJ-NPR-Z]|\d)(\d|X)([A-HJ-NPR-Z]+\d+|\d+[A-HJ-NPR-Z]+)\d{6}$#', $vin, $match);
	}

	return $returnValue;

}

/**
 * Validating email address
 */
function validate_email_address($email) {    
    return filter_var($email, FILTER_VALIDATE_EMAIL);
}
