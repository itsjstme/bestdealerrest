<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Ravi Tamada
 * @link URL Tutorial link
 */
class Dealer {


    private $conn;

    function __construct() {
        require_once dirname(__FILE__) . '/../../include/DbConnect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }

	public function manageCompany($args){

        $response = array();
    
        // First check if user already existed in db
        if ( empty($params["cid"]) ) {
            
            $params["status"]    = 1;
            $params["ipaddress"] = getIPAddress();
            $params["created"]   = date("Y-m-d H:i:s");     
            
            $sql = "INSERT INTO company (".implode("," , array_keys($params)).") VALUES (".implode( "," , array_map(function($value) { return ':' . $value; }, array_keys($params) ) ).")";
            // insert query
            $stmt = $this->conn->prepare($sql);
            foreach($params as $key => $value ){
                $stmt->bindValue($key, $value); //$stmt->bindParam(":".$key , $value );
            }

            $result = $stmt->execute();            
            // Check for successful insertion
            if ($result) {
                // User successfully inserted
                return SUCCESSFULLY;
            } else {
                // Failed to create user
                return FAILED;
            }
        } else {

        	$companyID			 = $params["cid"];
        	unset($params["cid"]);
            $params["ipaddress"] = getIPAddress();
    

            $sql = " UPDATE company SET " . implode( " , " , array_map(function($value) { return  $value.' = :' .$value; }, array_keys($params) ) ) . " WHERE cid = '{$companyID}' " ; 
            // insert query
            $stmt = $this->conn->prepare($sql);
            
            foreach($params as $key => $value ){
                $stmt->bindValue($key, $value); //$stmt->bindParam(":".$key , $value );
            }
            // $stmt->bindParam(":vin", $vinNumber);
            $result = $stmt->execute();            
            // Check for successful insertion
            if ($result) {
                // User successfully inserted
                return SUCCESSFULLY;
            } else {
                // Failed to create user
                return FAILED;
            }

        }

        return $response;


	}

	public function manageEmployee($args){

        $response = array();
    
        // First check if user already existed in db
        if ( empty($params["cid"]) ) {
            
            $params["status"]    = 1;
            $params["ipaddress"] = getIPAddress();
            $params["created"]   = date("Y-m-d H:i:s");     
            
            $sql = "INSERT INTO tbl_users (".implode("," , array_keys($params)).") VALUES (".implode( "," , array_map(function($value) { return ':' . $value; }, array_keys($params) ) ).")";
            // insert query
            $stmt = $this->conn->prepare($sql);
            foreach($params as $key => $value ){
                $stmt->bindValue($key, $value); //$stmt->bindParam(":".$key , $value );
            }

            $result = $stmt->execute();            
            // Check for successful insertion
            if ($result) {
                // User successfully inserted
                return SUCCESSFULLY;
            } else {
                // Failed to create user
                return FAILED;
            }
        } else {

        	$companyID			 = $params["cid"];
        	unset($params["cid"]);
            $params["ipaddress"] = getIPAddress();
    

            $sql = " UPDATE tbl_users SET " . implode( " , " , array_map(function($value) { return  $value.' = :' .$value; }, array_keys($params) ) ) . " WHERE cid = '{$companyID}' " ; 
            // insert query
            $stmt = $this->conn->prepare($sql);
            
            foreach($params as $key => $value ){
                $stmt->bindValue($key, $value); //$stmt->bindParam(":".$key , $value );
            }
            // $stmt->bindParam(":vin", $vinNumber);
            $result = $stmt->execute();            
            // Check for successful insertion
            if ($result) {
                // User successfully inserted
                return SUCCESSFULLY;
            } else {
                // Failed to create user
                return FAILED;
            }

        }

        return $response;


	}


	public static function GenerateVehicleData($args){
			
			$return 	= array();
			$contents 	= null;
				
			$rsEdmunds["data"] = json_decode($rs["data"], true);		
	
		if($rs['success']){
	
			$styleID  = $rsEdmunds["data"]['years'][0]['styles'][0]['id'];
			$fileName = STYLE_FOLDER . $styleID .'/results_'.$args["zip"].'.txt' ;
			
			if (file_exists($fileName)) {			
				$contents = file_get_contents ($fileName);
			}
			
			if(empty($contents)){
			
				$item				 = findInfo($args["zip"]);
				
				$params["yr"]        = $rsEdmunds["data"]['years'][0]['year'];
				$params["make"]      = $rsEdmunds["data"]['make']['name'];
				$params["model"]     = $rsEdmunds["data"]['model']['name'];		
				$params["styleID"]   = $rsEdmunds["data"]['years'][0]['styles'][0]['id'];								
				$params["zipcode"]   = $args["zip"];
				$params["keyword"]   = $params["yr"]." ".$params["make"]." ".$params["model"];
				$params["trim"]      = $rsEdmunds["data"]['years'][0]['styles'][0]['trim'];
				// Lilburn+GA-30047
				
				
				require_once dirname(__FILE__) . '\Ebay.php';
				//EBAY 				
				$ebay 			      = new Ebay(EBAY_APPID);
				$ebay->init($params);
				$rsEbay 			  = $ebay->findByKeywords( $params["keyword"]  );		

				if(isset($rsEbay['message']))
					$results['message'][] = $rsEbay['message'];
				
				if(isset($rsEbay["success"]) && $rsEbay["success"] == true ){
					$return["ebay"] 		= $rsEbay["data"];					
				}
				
				// CRAIGSLIST	
				if(isset($args["url"]))
					$params["url"]			= $args["url"];
				
				require_once dirname(__FILE__) . '\Craigslist.php';	
				$rsCraigsList			= CraigsList::Search($params);						
				
				if(isset($rsCraigsList["success"]) && $rsCraigsList["success"] == true )
					$return["cl"] = $rsCraigsList["data"];
									
				// AUTOTRADER
				// Lilburn+GA-30047				
				$params["zipComplete"] = urlencode($item->primary_city." ".$item->state."-".$params["zipcode"] );
				
				require_once dirname(__FILE__) . '\AutoTrader.php';
				
				$rsAutoTrader		  = AutoTrader::getVehicles($params);
				
				if(isset($rsAutoTrader["success"]) && $rsAutoTrader["success"] == true )
					$return["autotrader"] = $rsAutoTrader["data"];					
				
				$return["carDetails"] = $params["yr"]." ".$params["make"]." ".$params["model"];
				
				//   all prices
				if(isset($return["ebay"]) && !empty($return["ebay"])){				
					foreach($return["ebay"] as $key => $theValue){
						if(isset($theValue["price"]) && !empty($theValue["price"])){
							$prices[] = (float)$theValue["price"];
						}
						if(isset($theValue["mileage"]) && !empty($theValue["mileage"])){
							$miles[] = (float)$theValue["mileage"];
						}						
						// $miles[] = (float)$theValue["miles"];
					}	
				}
				
				
				if(isset($return["cl"]) && !empty($return["cl"])){				
					foreach($return["cl"] as $key => $theValue){
						if(isset($theValue["price"]) && !empty($theValue["price"])){
							$prices[] = (float)$theValue["price"];
						}
						if(isset($theValue["mileage"]) && !empty($theValue["mileage"])){
							$miles[] = (float)$theValue["mileage"];
						}							
						// $miles[] = (float)$theValue["miles"];
					}	
				}
				if(isset($return["autotrader"]) && !empty($return["autotrader"])){			
					foreach($return["autotrader"] as $key => $theValue){
						if(isset($theValue["price"]) && !empty($theValue["price"])){
							$prices[] = (float)$theValue["price"];
						}
						if(isset($theValue["mileage"]) && !empty($theValue["mileage"])){
							$miles[] = (float)$theValue["mileage"];
						}							
						// $miles[] = (float)$theValue["miles"];						
					}
				}						
				
				if(isset($prices))
					$return["price"]  		= $prices;	
				
				if(isset($miles))
					$return["miles"]  		= $miles;	
												
				file_put_contents($fileName , json_encode($return)  );
											
			}
			else
				$return = json_decode($contents , true);
			
		}	
	
		return $return;
	
	
	}


}


?>