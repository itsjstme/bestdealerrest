<?php

	class Ebay 
	{
		const FINDING_API_URL = 'http://svcs.ebay.com/services/search/FindingService/v1?';

		private $appId;
		private $params;
		

		public function init($params = null){
						
			if(!is_null($params))
				$this->params = $params;
			else
				die('Style ID required');		
		}		

		public function __construct($appId)
		{
			$this->appId = $appId;
		}

		public function findByProduct($id, $type = 'UPC')
		{
			$params = array(
				'productId.@type' => $type,
				'productId' => $id,
			);

			return $this->doApiRequest('findItemsByProduct', $params);
		}

		public function findByKeywords($keywords)
		{
			$params = array(
				'keywords' => $keywords,
			);

			$rs =  $this->doApiRequest('findItemsAdvanced', $params) ;
			$records = json_decode($rs["data"]);
			
			if(isset($records->findItemsAdvancedResponse)){
						
				foreach($records->findItemsAdvancedResponse[0]->searchResult[0]->item as $item){
						
						if(isset($item->postalCode[0]) && !empty($item->postalCode[0]))
							$items[$item->itemId[0]]["zip"] = $item->postalCode[0];
							
						if(isset($item->attribute[1]->value[0]) && !empty($item->attribute[1]->value[0]))	
							$items[$item->itemId[0]]["mileage"] = $item->attribute[1]->value[0];
							
						if(isset($item->sellingStatus[0]->convertedCurrentPrice[0]->__value__) && !empty($item->sellingStatus[0]->convertedCurrentPrice[0]->__value__))
							$items[$item->itemId[0]]["price"] = $item->sellingStatus[0]->convertedCurrentPrice[0]->__value__;
				}		
			
				$results["success"] = is_null($items)?FALSE:TRUE;
				$results["data"] 	= $items;		
			
			}
			
			return $results;			
		}


		private function doApiRequest($operationName, $payload)
		{

			$global = array(
				'OPERATION-NAME' => $operationName,
				'SECURITY-APPNAME' => $this->appId,
				'GLOBAL-ID' => 'EBAY-US',
				'SERVICE-VERSION' => '1.0.0',
				'MESSAGE-ENCODING' => 'UTF-8',
				'RESPONSE-DATA-FORMAT' => 'JSON',
			);

			$url 	= self::FINDING_API_URL . http_build_query($global) . '&REST-PAYLOAD&' . http_build_query($payload);
			$ret 	= Processes::processURL($url , 'EBAY' , STYLE_FOLDER .'/'.$this->params["styleID"].'/Ebay_'. date("Ymd") , $this->params["styleID"] );
			
			return $ret;
		}

	}

?>