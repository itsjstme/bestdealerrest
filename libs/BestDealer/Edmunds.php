<?php
// Yii extension created by Marco Troisi
// Based on Simon Willison's php XMLWriter class
// Based on Lars Marius Garshol's Python XMLWriter class
// See http://www.xml.com/pub/a/2003/04/09/py-xml.html
// For any question: hello@marcotroisi.com

class Edmunds {

		public static $apiKey ;
		public static $baseFolder ;
		
		public static function init() {

			self::$apiKey = 'rpwhm99xyh8wqnzm2hebqse9'; 
			
			if(empty(self::$apiKey))
				die('API Needed');
				
			self::$baseFolder = DATA_FOLDER ;	
		}		
	
		public static function  GetVehicleRecallInfo($ModelYearID){
				
			self::init();
	
			require_once dirname(__FILE__) . '/../../include/DbConnect.php';
			$db = new DbConnect();
        	$conn = $db->connect();

			$json	= NULL;
			
			$url 		= "https://api.edmunds.com/v1/api/maintenance/recallrepository/findbymodelyearid?modelyearid={$ModelYearID}&fmt=json&api_key={self::$apiKey}";						
	        $stmt = $conn->prepare("SELECT url , results  FROM edmunds WHERE url = :url");
			$stmt->bindParam(":url", $url);
			$stmt->execute(); 
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$json = $results[0]['results'];
			
			
			if(empty($json)){
	
					$params['results'] =  $json = file_get_contents( $url , true);
					$params['url'] = $url;
					return $db->insert($params , 'edmunds' );

			}
			
			return true;
		}
		
		public static function  GetVehiclePhoto($styleID){

				self::init();
				 
				require_once dirname(__FILE__) . '/../../include/DbConnect.php';
				$db = new DbConnect();
	        	$conn = $db->connect();
	
				$json	= NULL;
				
				$url 		= "https://api.edmunds.com/v1/api/vehiclephoto/service/findphotosbystyleid?styleId={$styleID}&fmt=json&api_key={self::$apiKey}";						
		        $stmt = $conn->prepare("SELECT url , results  FROM edmunds WHERE url = :url");
				$stmt->bindParam(":url", $url);
				$stmt->execute(); 
				$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				$json = $results[0]['results'];
				
				
				if(empty($json)){
		
						$params['results'] =  $json = file_get_contents( $url , true);
						$params['url'] = $url;
						return $json;
	
				}
				
				return $json;				
			
		}


		public static function  GetAutoValueByModelMakeYear($styleID){
				
				self::init();
				
				require_once dirname(__FILE__) . '/../../include/DbConnect.php';
				$db = new DbConnect();
	        	$conn = $db->connect();
	
				$json	= NULL;
				
				$url 		= "https://api.edmunds.com/v1/api/vehiclephoto/service/findphotosbystyleid?styleId={$styleID}&fmt=json&api_key={self::$apiKey}";						
		        $stmt = $conn->prepare("SELECT url , results  FROM edmunds WHERE url = :url");
				$stmt->bindParam(":url", $url);
				$stmt->execute(); 
				$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				$json = $results[0]['results'];
				
				
				if(empty($json)){
		
						$params['results'] =  $json = file_get_contents( $url , true);
						$params['url'] = $url;
						return $json;
	
				}
				
				return $json;				
		
		}	

		
		public static function  GetCertifiedVehicleCost($styleID , $zipCode){

				$jsondata 	= false;
				$url 		= "https://api.edmunds.com/v1/api/tmv/tmvservice/findcertifiedpriceforstyle?styleid={$styleID}&zip={$zipCode}&fmt=json&api_key={self::$apiKey}";
				$jsondata 	= file_get_contents( $url , true);
				return $jsondata;
		
		}

		public static function  GetUsedVehicleResaleValue($styleID , $mileage , $zipCode){

				
					require_once dirname(__FILE__) . '/../../include/DbConnect.php';
					$db = new DbConnect();
		        	$db->connect();
		
					$urls["Outstanding"] = "https://api.edmunds.com/v1/api/tmv/tmvservice/calculateusedtmv?styleid={$styleID}&condition=Outstanding&mileage={$mileage}&zip={$zipCode}&fmt=json&api_key={self::$apiKey}";					
					$urls["Clean"] 		= "https://api.edmunds.com/v1/api/tmv/tmvservice/calculateusedtmv?styleid={$styleID}&condition=Clean&mileage={$mileage}&zip={$zipCode}&fmt=json&api_key={self::$apiKey}";	
					$urls["Average"] 	= "https://api.edmunds.com/v1/api/tmv/tmvservice/calculateusedtmv?styleid={$styleID}&condition=Average&mileage={$mileage}&zip={$zipCode}&fmt=json&api_key={self::$apiKey}";	
					$urls["Rough"] 		= "https://api.edmunds.com/v1/api/tmv/tmvservice/calculateusedtmv?styleid={$styleID}&condition=Rough&mileage={$mileage}&zip={$zipCode}&fmt=json&api_key={self::$apiKey}";	
					$urls["Damaged"] 	= "https://api.edmunds.com/v1/api/tmv/tmvservice/calculateusedtmv?styleid={$styleID}&condition=Damaged&mileage={$mileage}&zip={$zipCode}&fmt=json&api_key={self::$apiKey}";
					
					foreach ($urls as $key => $url) {
						
						$json	= NULL;
				        $stmt		= $conn->prepare("SELECT url , results  FROM edmunds WHERE url = :url");
						$stmt->bindParam(":url", $url);
						$stmt->execute(); 
						$results	= $stmt->fetchAll(PDO::FETCH_ASSOC);
						$json		= $results[0]['results'];
						
						if(empty($json)){
				
								$params['results'] =  $json = file_get_contents( $url , true);
								$params['url'] = $url;
								$db->insert($params , 'edmunds' );
			
						}
						
						$results[$key] =  $json;
						sleep(5);
					}
				
				return $results;
		}			
	
	
		public static function  findCarByVIN($vinNumber) {		
			
			self::init();
			
			require_once dirname(__FILE__) . '/../../include/DbConnect.php';
			$db = new DbConnect();
        	$conn = $db->connect();

			$json	= NULL;
			
			$url  = "https://api.edmunds.com/api/vehicle/v2/vins/{$vinNumber}?fmt=json&api_key=rpwhm99xyh8wqnzm2hebqse9";						
	        $stmt = $conn->prepare("SELECT url , results  FROM edmunds WHERE url = :url");
			$stmt->bindParam(":url", $url);
			$stmt->execute(); 
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$json = $results[0]['results'];
			
			
			if(empty($json)){
	
					$params['results'] =  $json = @file_get_contents( $url , true);
					$params['url'] = $url;
					
					if(!empty($json)){
			            $sql = "INSERT INTO `edmunds` (".implode("," , array_keys($params)).") VALUES (".implode( "," , array_map(function($value) { return ':' . $value; }, array_keys($params) ) ).")";
			            // insert query
			            $stmt = $conn->prepare($sql);
						
			            foreach($params as $key => $value ){
			                $stmt->bindValue($key, $value); //$stmt->bindParam(":".$key , $value );
			            }			
	
			            $stmt->execute();  
					}
			}
			
			return json_decode($json  ,true);
		
		}	
    
}
?>  
