<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Ravi Tamada
 * @link URL Tutorial link
 */
class Inventory {

    private $conn;

    function __construct() {
        require_once dirname(__FILE__) . '/../../include/DbConnect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }

	
    /**
     * Creating new user
     * @param String $name User full name
     * @param String $email User login email id
     * @param String $password User login password
     */
    public function prepVehicle($params) {

			$response = array();
    
			// First check if user already existed in db               
            $params["status"]    = 1;
            $params["created"]   = date("Y-m-d H:i:s");                 
            // insert query
			
			$result = $db->insert($params , 'inventory_expenses' );
			/*
			$sql = "INSERT INTO `inventory_expenses` (".implode("," , array_keys($params)).") VALUES (".implode( "," , array_map(function($value) { return ':' . $value; }, array_keys($params) ) ).")";
			// insert query
			$stmt = $this->conn->prepare($sql);
			
			foreach($params as $key => $value ){
				$stmt->bindValue($key, $value); //$stmt->bindParam(":".$key , $value );
			}
			
			$result = $stmt->execute();	
			*/
			
            if ($result) {
                $response["error"] = SUCCESSFULLY;
                $response["message"] = "Added ";                

            } else {
                $response["error"] = FAILED;
                $response["message"] = "Failed";                

            }			
			

			return $response;
    }	

    /**
		* Creating new user
     * @param String $name User full name
     * @param String $email User login email id
     * @param String $password User login password
     */
    public function addVehicle($params) {

        $response = array();
    
        // First check if user already existed in db
            
		$params["status"]    = 1;
		$params["ipaddress"] = getIPAddress();
		$params["created"]   = date("Y-m-d H:i:s");                 
		
        // $sql = " SELECT *  FROM view_inventory WHERE userid = '{$userID}' AND status > -1   {$where} " ; 
		$sql = " SELECT 1 FROM inventory WHERE createdBy = '{$params['createdBy']}' AND  styleid = '{$params['styleid']}'   " ; 
        // insert query
        $stmt = $this->conn->prepare($sql);
        // $stmt->bindParam(":vin", $vinNumber);
        $result = $stmt->execute();            
        // Check for successful insertion
        $count = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        if(count($count) > 0  ){
			$response["error"] = FAILED;
			$response["message"] = "Vehicle already exists";                
		}else{
        
    		// insert query
    		
    		/*
    		$sql = "INSERT INTO inventory (".implode("," , array_keys($params)).") VALUES (".implode( "," , array_map(function($value) { return ':' . $value; }, array_keys($params) ) ).")";
    		// insert query
    		$stmt = $this->conn->prepare($sql);
    		
    		foreach($params as $key => $value ){
    			$stmt->bindValue($key, $value); //$stmt->bindParam(":".$key , $value );
    		}
    		
    		$result = $stmt->execute();  
    		*/
    		
    		$result = $db->insert($params , 'inventory' );
    		
    		// Check for successful insertion
    		if ($result) {
    			$response["error"] = SUCCESSFULLY;
    			$response["message"] = "Vehicle successfully added ";                
    
    		} else {
    			$response["error"] = FAILED;
    			$response["message"] = "Vehicle not added";                
    
    		}
		
		}
        
        return $response;
    }	
	
    /**
     * Creating new user
     * @param String $name User full name
     * @param String $email User login email id
     * @param String $password User login password
     */
    public function editVehicle($params , $autoid ) {

        $response = array();
    
        // First check if user already existed in db            
		$params["status"]    = 1;
		$params["ipaddress"] = getIPAddress();   

		/* 
		$sql = " UPDATE inventory SET " . implode( " , " , array_map(function($value) { return  $value.' = :' .$value; }, array_keys($params) ) ) . " WHERE autoid = '{$autoid}' " ; 

		// insert query
		$stmt = $this->conn->prepare($sql);
		*/
		
		$result = $db->update($params , 'inventory' , " WHERE autoid = '{$autoid}' " );
		
		foreach($params as $key => $value ){
			$stmt->bindValue($key, $value); //$stmt->bindParam(":".$key , $value );
		}
		// $stmt->bindParam(":vin", $vinNumber);
		$result = $stmt->execute();            
		// Check for successful insertion
		if ($result) {
			$response["error"] = SUCCESSFULLY;
			$response["message"] = "Vehicle successfully updated ";                

		} else {
			$response["error"] = FAILED;
			$response["message"] = "Failed to updated vehicle";                

		}


        return $response;
    }   

    /**
     * Creating new user
     * @param String $name User full name
     * @param String $email User login email id
     * @param String $password User login password
     */
    public function deleteVehicle($autoid) {

        $response = array();
    
        // First check if user already existed in db

            
		// $vinNumber           = $params["vin"];
		// $createdBy           = $params["createdBy"];
		$sql 				 = " UPDATE inventory set STATUS = -1  WHERE autoid = :autoid" ; 
		
		// insert query
		$stmt = $this->conn->prepare($sql);
		// $stmt->bindParam(":vin", $vinNumber);
		
		$stmt->bindParam(":autoid", $autoid ); 
		
		$result = $stmt->execute();            
		// Check for successful insertion
		if ($result) {
			$response["error"] = SUCCESSFULLY;
			$response["message"] = "Vehicle successfully deleted ";                

		} else {
			$response["error"] = FAILED;
			$response["message"] = "Failed in deleting vehicle";                

		}


        return $response;
    }

    /**
     * Creating new user
     * @param String $name User full name
     * @param String $email User login email id
     * @param String $password User login password
     */
    public function getVehicles($userID , $autoid = null , $favs = false ) {
        
		
        $where =   "";
        $limit = "AND status > -1  ORDER by purchasedate DESC LIMIT 100";
        
        if($autoid > 0){
			$where .= " AND autoid = '{$autoid}' ";
            $limit ="";
        }  

        if($favs){
            $where .= " starred = 1 ";
        }
		        
        // $sql = " SELECT *  FROM view_inventory WHERE userid = '{$userID}' AND status > -1   {$where} " ; 
		$sql = " SELECT * , concat(vin , ' ' , year, ' ' , make, ' ' , model, ' ' , body, ' ' , trim, ' ' , name, ' ' , mileage , ' ' , purchaseprice ) as searchOn FROM inventory WHERE cid = '{$userID}' {$where}  {$limit} " ; 
        // insert query
        $stmt = $this->conn->prepare($sql);
        // $stmt->bindParam(":vin", $vinNumber);
        $result = $stmt->execute();            
        // Check for successful insertion
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

  /**
     * Creating new user
     * @param String $name User full name
     * @param String $email User login email id
     * @param String $password User login password
     */
    public function getSearchResults($search , $limit = null) {
        
        if(is_null($null))
            $limit = "LIMIT 50";
        else
            $limit = 'LIMIT '.$limit;
            
        // $sql = " SELECT *  FROM view_inventory WHERE userid = '{$userID}' AND status > -1   {$where} " ; 
		$sql = " SELECT * FROM `view_make_model_mmid` WHERE searchOn LIKE '%{$search}%' {$limit} " ; 
        // insert query
        $stmt = $this->conn->prepare($sql);
        // $stmt->bindParam(":vin", $vinNumber);
        $result = $stmt->execute();            
        // Check for successful insertion
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
	
	public function getVehicleByStyleID($styleID){
		
        $sql = " SELECT * FROM make_model WHERE styleID =  {$styleID} " ; 
        // insert query
        $stmt = $this->conn->prepare($sql);
        // $stmt->bindParam(":vin", $vinNumber);
        $result = $stmt->execute();            
        // Check for successful insertion
        return $stmt->fetchAll(PDO::FETCH_ASSOC);		
		
	}
	
	public function getFeeds($userId){
		
        $sql = " SELECT * FROM `view_active_offers` as A LEFT JOIN `view_counter_offer_details` as B ON A.id = B.offerid WHERE createdBy !=  {$userId} " ; 
        // insert query
        $stmt = $this->conn->prepare($sql);
        // $stmt->bindParam(":vin", $vinNumber);
        $result = $stmt->execute();            
        // Check for successful insertion
        return $stmt->fetchAll(PDO::FETCH_ASSOC);		
		
	}	
	
	public function getOffers($userId){
		
        $sql = " SELECT * FROM `view_active_offers` WHERE createdBy !=  {$userId} " ; 
        // insert query
        $stmt = $this->conn->prepare($sql);
        // $stmt->bindParam(":vin", $vinNumber);
        $result = $stmt->execute();            
        // Check for successful insertion
        return $stmt->fetchAll(PDO::FETCH_ASSOC);		
		
	}	

	public function getCounterOffers($autoid){
		
        $sql = " SELECT * FROM `view_counter_offers` WHERE offerid =  {$autoid} " ; 
        // insert query
        $stmt = $this->conn->prepare($sql);
        // $stmt->bindParam(":vin", $vinNumber);
        $result = $stmt->execute();            
        // Check for successful insertion
        return $stmt->fetchAll(PDO::FETCH_ASSOC);		
		
	}	

    public function submitCounterOffer($params){


			$response = array();
    
			// First check if user already existed in db               
            $params["status"]    = 1;
            $params["created"]   = date("Y-m-d H:i:s");                 
            // insert query
			/*
			$sql = "INSERT INTO `inventory_offer_counter` (".implode("," , array_keys($params)).") VALUES (".implode( "," , array_map(function($value) { return ':' . $value; }, array_keys($params) ) ).")";
			// insert query
			$stmt = $this->conn->prepare($sql);
			
			foreach($params as $key => $value ){
				$stmt->bindValue($key, $value); //$stmt->bindParam(":".$key , $value );
			}
			
			$result = $stmt->execute();	
			*/
			
			$result = $db->insert($params , 'inventory_offer_counter' );
			
            if ($result) {
                $response["error"] = SUCCESSFULLY;
                $response["message"] = "Added ";                

            } else {
                $response["error"] = FAILED;
                $response["message"] = "Failed";                

            }			
			

			return $response;
        
    }


    /**
     * Creating new user
     * @param String $name User full name
     * @param String $email User login email id
     * @param String $password User login password
     */
    public function vehicleStatus($params , $companyID  ) {
        
        $option = "";
        
        if(isset($params["soldprice"]) && $params["soldprice"] > 0 ){
            $option ="soldprice = :soldprice ,";
        }
        
        $sql = " UPDATE inventory SET  {$option} solddate = :solddate , status = :status WHERE autoid = :autoid AND userid = :userid " ; 
        // insert query
        $stmt = $this->conn->prepare($sql);
        
        if(isset($params["soldprice"]) && $params["soldprice"] > 0 ){
            $stmt->bindParam(":soldprice", $params["soldprice"] );
        }
        
        $stmt->bindParam(":solddate", date( 'Y-m-d H:i:s') );
        $stmt->bindParam(":status", $param["status"] ); 
        $stmt->bindParam(":userid", $companyID );           
        $stmt->bindParam(":autoid", $params["autoid"] );   
        
        
        
        // Check for successful insertion
        return $stmt->execute();
    }   
        
    /**
     * Checking for duplicate user by email address
     * @param String $email email to check in db
     * @return boolean
     */
    private function isVinExists($vinNumber) {
        $stmt = $this->conn->prepare("SELECT * from inventory WHERE vin = :vin");
        $stmt->bindParam(":vin", $vinNumber);
        $stmt->execute();
        $results = $stmt->fetchAll();
        return (count($results) >= 1);
    }





}


?>