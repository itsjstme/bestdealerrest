<?php



require_once dirname(__FILE__) . '\..\libs\Payment\init.php';


class Payment {
	
    private $conn;
	private $keyID;

    function __construct() {
        require_once dirname(__FILE__) . '\DbConnect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
		$this->keyID = (( isset($_SERVER["SERVER_NAME"]) && $_SERVER["SERVER_NAME"] != "localhost.api.bestdealer"))?PROD_STRIPE_API_KEY:DEV_STRIPE_API_KEY;
		\Stripe\Stripe::setApiKey($this->keyID);
	}
	
	private function createToken($params){
			
			//creates a token
			$result = \Stripe\Token::create(
				array(
					"card" => array(
						"name" => $params['name'],
						"number" => $params['card_number'],
						"exp_month" => $params['month'],
						"exp_year" => $params['year'],
						"cvc" => $params['cvc_number'],
						"name" => $params['cvc_number'],
						"address_line1" => $params['cvc_number'],
						"address_line2" => $params['address_line2'],
						"address_city" => $params['address_city'],
						"address_state" => $params['address_state'],
						"address_zip" => $params['address_zip'],
						"address_country" => $params['address_country'],

						
					)
				)
			);		
		
			if($result['id'])
				return $result['id'];
			else
				return NULL;
	}
	
	public function manageCustomer($params){
			
            global $user_id;
            global $cid;	
		
			$response["error"] = true;
			$errors            = '';		
			
			$this->conn->beginTransaction();
			
			try {
				// Create a Customer Token
				$token = $this->createToken($params);
				// creates a new customer
				if(!isset($params['token'])){
					
					$customer = \Stripe\Customer::create(array(
					  "source" => $token ,
					  "description" => $user_id,
					  "email" => $params['email'],
					  "plan" => $params['plan'],

					  )
					);
					$stmt = $this->conn->prepare("UPDATE company SET stripeToken = :token WHERE cid = :cid ");
					// $stmt->bindParam(":name", $name);
					$stmt->bindParam(":token", $token);	
					$stmt->bindParam(":cid", $cid);		
					$result = $stmt->execute();  									

				}
				// updates a new customer
				if($params['token']){
					
					$cu = \Stripe\Customer::retrieve($params['token']);
				
					$cu = \Stripe\Customer::retrieve("cus_6LHNREhiMnZVtP");
					$cu->description = $user_id;
					$cu->source = $token; // obtained with Stripe.js
					
					$stmt = $this->conn->prepare("UPDATE company SET stripeToken = :token WHERE cid = :cid ");
					// $stmt->bindParam(":name", $name);
					$stmt->bindParam(":token", $token);	
					$stmt->bindParam(":cid", $cid);		
					$result = $stmt->execute();  				
					$response["error"] = $stmt->execute();					
					$cu->save();
				}				
				
				
				$this->conn->commit();
			} catch (\Stripe\CardError $e) {
				// Since it's a decline, Stripe_CardError will be caught
				$body = $e->getJsonBody();
				$errors  = $body['error'];

				$errors =. 'Status is:' . $e->getHttpStatus() . "\n";
				$errors =. 'Type is:' . $err['type'] . "\n";
				$errors =. 'Code is:' . $err['code'] . "\n";
				// param is '' in this case
				$errors =. 'Param is:' . $err['param'] . "\n";
				$errors =. 'Message is:' . $err['message'] . "\n";
				$this->conn->rollBack();
			} catch (Exception $e) {
				$errors =. $e->getMessage();
				 $this->conn->rollBack();
			} 		
		
		
	}
	
	public function deleteCustomer($params , $cid ){
		
		try {
			$cu = \Stripe\Customer::retrieve($params['token']);
			$customer = $cu->delete();	
			
			if($customer["deleted"]){

				$stmt = $this->conn->prepare("UPDATE company SET status =  " . ARCHIVE . " WHERE cid = :cid ");
				$stmt->bindParam(":cid", $cid);
				$stmt->execute(); 
				
				$stmt = $this->conn->prepare("UPDATE company SET status =  " . ARCHIVE . " WHERE cid = :cid ");
				$stmt->bindParam(":cid", $cid);
				$stmt->execute(); 				
			
			}
			
			$this->conn->commit();
			
		} catch (Exception $e) {
			$errors =. $e->getMessage();
			$this->conn->rollBack();
		}
		
		
	}	
	
	public function processPayment($params , $cid ){
		
		try {
			$cu = \Stripe\Customer::retrieve($params['token']);
			$customer = $cu->delete();	
			
			if($customer["deleted"]){

				$stmt = $this->conn->prepare("UPDATE company SET status =  " . ARCHIVE . " WHERE cid = :cid ");
				$stmt->bindParam(":cid", $cid);
				$stmt->execute(); 
				
				$stmt = $this->conn->prepare("UPDATE company SET status =  " . ARCHIVE . " WHERE cid = :cid ");
				$stmt->bindParam(":cid", $cid);
				$stmt->execute(); 				
			
			}
			
			$this->conn->commit();
			
		} catch (Exception $e) {
			$errors =. $e->getMessage();
			$this->conn->rollBack();
		}
		
		
	}	
	
}
