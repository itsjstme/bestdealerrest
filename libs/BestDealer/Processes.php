<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Ravi Tamada
 * @link URL Tutorial link
 */
class Processes {

    private static $conn = NULL;

	
	
    public static function dbConnect() {
        require_once dirname(__FILE__) . '/../../include/DbConnect.php';
        // opening db connection
        $db = new DbConnect();
        self::$conn = $db->connect();
    }


	public static function findInfo($zipCode){
	

		if (file_exists( XML_GEO )) {
			
			$xml = simplexml_load_file( XML_GEO );				
			$zip = $xml->xpath('/ROWSET/ROW[zip="'.$zipCode.'"]');
			
			if(isset($zip) && !empty($zip))
				return $zip[0];		
		 
		} 
		
		return false;
	
	}
	
	public static function findCraigslist($zipCode){
	

		if (file_exists(XML_CRAIGSLIST)) {
			
			$xml = simplexml_load_file( XML_CRAIGSLIST );			
			$zip = $xml->xpath('/ROWSET/ROW[zip="'.$zipCode.'"]');
			
			if(isset($zip[0]))
				return $zip[0];
		 
		} 
			
			return false;
	
	}	
	
	public static function GetZipFromState($state){
	

		if (file_exists( XML_GEO )) {

			$xml = simplexml_load_file( XML_GEO );			
			$results = $xml->xpath('/ROWSET/ROW[state="'.$state.'"]');
			
			foreach($results as $item){
				$zips[(string) $item->zip] = 0; 
			 }//$zips[(string) $item->zip] = 0;
		 	
			return $zips;
		} 
			
			return false;
	
	}	
	
	public static function addCronApi($params){
		
		if ( is_null( self::$conn ) ){
		  self::dbConnect();
		}		
		
		$params["status"] 	 = 1;
		$params["ipaddress"] = getIPAddress();
		$params["created"]   = date("Y-m-d H:i:s");		
		
		$sql = "INSERT INTO cron_api_queries (".implode("," , array_keys($params)).") VALUES (".implode( "," , array_map(function($value) { return ':' . $value; }, array_keys($params) ) ).")";
		
		// insert query
		$stmt = self::$conn->prepare($sql);
		foreach($params as $key => $value ){
			$stmt->bindValue($key, $value); //$stmt->bindParam(":".$key , $value );
		}

		$result =  $stmt->execute(); 
		return $result;
	
	}
	
	public static function processURL($url , $api , $fileName , $styleID ='' , $dontAppendToFile = true  , $siteID = 0 ){

		if ( is_null( self::$conn ) ){
		  self::dbConnect();
		}
	
		$results["success"] = FALSE;
		$results["message"] = '';
		$results["data"]	= NULL;		
						
		// check if query already executed for a date
		// if has get the file if not curl and save file
		
        $stmt = self::$conn->prepare("SELECT api_key FROM cron_api_queries WHERE api = :api AND url = :url");
        $stmt->bindParam(":api", $api);
		$stmt->bindParam(":url", $url);
		
        if ($stmt->execute()) {			
            $rs 				= $stmt->fetch();
            $results["data"] 	= file_get_contents($fileName);
        }						
		
		if( is_null($results["data"]) ){

			if (!empty($styleID) && !file_exists( STYLE_FOLDER . $styleID)) {							
				if (!mkdir( STYLE_FOLDER . $styleID, 0777, true)) {
					$results["message"] =  "CANT CREATE FOLDER CHEK PERMISSION." . STYLE_FOLDER . $styleID ;					
					return $results;
				}	
				else
					$results["message"] =  "Loaded from file." . $fileName ;				
			}			
			
			$opts           = array('http'=>array('header' => "User-Agent:MyAgent/1.0\r\n"));
			$context        = stream_context_create($opts);					
			$xml 	  		= file_get_contents($url,false,$context);
			
			$args["styleID"] = $styleID;
			$args["api"]     = $api;
			$args["url"]     = $url;
			
			if($siteID > 0 )
				$args["siteID"]  =$siteID;
			
			$results         = self::addCronApi($args);
			
			if( ! $results ){
				$results["message"] = "Could save to table." ;
			}
							
			if(!empty($xml)){ 
				// print_r($xml);
				if($dontAppendToFile)		
					$cfile   = file_put_contents($fileName , preg_replace('/[^(\x20-\x7F)]*/',"", $xml )  );
				else
					$cfile   = file_put_contents($fileName , preg_replace('/[^(\x20-\x7F)]*/',"", $xml )  , FILE_APPEND | LOCK_EX );
				
				if($cfile == false){
					$results["message"] = 'File not created -- ' . $fileName;
				} else{	
					$results["message"] = 'Didnt write to file query';	
				}
				
				
				$results["data"] 	= $xml;
				
			}	
			else
				$results["message"] = 'No data found';	
								
			
		}

		$results["success"] = is_null($results["data"])?FALSE:TRUE;	
		return $results;
		
	}		


	public static function GenerateVehicleData($rsEdmunds , $zipCode , $fileName ,  $url = null ){
			
			$return = array();
				
			require_once dirname(__FILE__) . 'Edmunds.php';
			require_once dirname(__FILE__) . 'Craigslist.php';
			require_once dirname(__FILE__) . 'Ebay.php';
			require_once dirname(__FILE__) . 'AutoTrader.php';
			require_once dirname(__FILE__) . 'Cars.php';
			require_once dirname(__FILE__) . 'CarsGuru.php';
	
		if($rsEdmunds['success']){
			
			$styleID  = $rsEdmunds['styleID'];				
			$contents = file_get_contents ($fileName);
			
			if(empty($contents)){
			
				$item				 = findInfo($zipCode);
				
				$params["yr"]        = $rsEdmunds['year'];
				$params["make"]      = $rsEdmunds['make'];
				$params["model"]     = $rsEdmunds['model'];		
				$params["styleID"]   = $rsEdmunds['styleID'];								
				$params["zipcode"]   = $zipCode;					
				// Lilburn+GA-30047
				print_r($rsEdmunds["data"]);
				die();
				
				//EBAY 				
				$ebay 			      = new Ebay($params["styleID"]);
				$rsEbay 			  = $ebay->findItemsAdvanced( $params["yr"]." ".$params["make"]." ".$params["model"] , 'BestMatch' , 'FixedPricedItem' , 0 , 100000 , 100 );										
				$results['message'][] = $rsEbay['message'];
				
				if(isset($rsEbay["success"]) && $rsEbay["success"] == true ){
					$return["ebay"] 		= $rsEbay["data"];					
				}
				
				
				// CRAIGSLIST	
				if(!is_null($url))
					$params["url"]			= $url;
					
				$cl 					= CraigsList::init($params["styleID"]);
				$rsCraigsList			= CraigsList::Search($params);						
				
				if(isset($rsCraigsList["success"]) && $rsCraigsList["success"] == true )
					$return["cl"] = $rsCraigsList["data"];
									
				// AUTOTRADER
				// Lilburn+GA-30047
				$params["zipComplete"] = urlencode($item->primary_city." ".$item->state."-".$zipCode);
				
				$autoTrader 		  = new AutoTrader($params);
				$rsAutoTrader		  = $autoTrader->getVehicles();
				
				if(isset($rsAutoTrader["success"]) && $rsAutoTrader["success"] == true )
					$return["autotrader"] = $rsAutoTrader["data"];					
				
				$return["carDetails"] = $params["yr"]." ".$params["make"]." ".$params["model"];
				
				//   all prices
				
				if(isset($return["ebay"]) && !empty($return["ebay"])){				
					foreach($return["ebay"] as $key => $theValue){
						if(isset($theValue["price"]) && !empty($theValue["price"])){
							$prices[] = (float)$theValue["price"];
						}
						if(isset($theValue["miles"]) && !empty($theValue["miles"])){
							$miles[] = (float)$theValue["miles"];
						}						
						// $miles[] = (float)$theValue["miles"];
					}	
				}
				
				if(isset($return["cl"]) && !empty($return["cl"])){				
					foreach($return["cl"] as $key => $theValue){
						if(isset($theValue["price"]) && !empty($theValue["price"])){
							$prices[] = (float)$theValue["price"];
						}
						if(isset($theValue["miles"]) && !empty($theValue["miles"])){
							$miles[] = (float)$theValue["miles"];
						}							
						// $miles[] = (float)$theValue["miles"];
					}	
				}
				if(isset($return["autotrader"]) && !empty($return["autotrader"])){			
					foreach($return["autotrader"] as $key => $theValue){
						if(isset($theValue["price"]) && !empty($theValue["price"])){
							$prices[] = (float)$theValue["price"];
						}
						if(isset($theValue["miles"]) && !empty($theValue["miles"])){
							$miles[] = (float)$theValue["miles"];
						}							
						// $miles[] = (float)$theValue["miles"];						
					}
				}						
				
				$return["price"]  		= $prices;	
				// $return["miles"]		= $miles;				
				$cfile   				= Yii::app()->files->file($fileName , true );
				
				if($cfile)
				   Yii::app()->files->write( json_encode($return) );
			
			}
			else
				$return = json_decode($contents , true);
			
		}	
	
		return $return;
	
	
	}


}


?>