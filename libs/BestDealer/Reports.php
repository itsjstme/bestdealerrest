<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Ravi Tamada
 * @link URL Tutorial link
 */
class Reports {

    private $conn;

    function __construct() {
        require_once dirname(__FILE__) . '/../../include/DbConnect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }


    /**
     * Creating new user
     * @param String $name User full name
     * @param String $email User login email id
     * @param String $password User login password
     */
    public  function getFinanceDashboard($userid) {
		
		
		$sql = " SELECT * FROM view_financial_dashboard WHERE userId = '{$userid}' " ; 
		// insert query
        $stmt = $this->conn->prepare($sql);
        // $stmt->bindParam(":vin", $vinNumber);
        $result = $stmt->execute();            
        // Check for successful insertion
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
	
    /**
     * Creating new user
     * @param String $name User full name
     * @param String $email User login email id
     * @param String $password User login password
     */
    public  function getFinance($companyID) {
		
		
		$sql = " SELECT * FROM view_financial_dashboard WHERE cid = '{$companyID}' " ; 
		// insert query
        $stmt = $this->conn->prepare($sql);
        // $stmt->bindParam(":vin", $vinNumber);
        $result = $stmt->execute();            
        // Check for successful insertion
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }


}


?>