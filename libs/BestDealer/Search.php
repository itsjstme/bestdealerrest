<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Ravi Tamada
 * @link URL Tutorial link
 */
class Search {


    private $conn;

    function __construct() {
        require_once dirname(__FILE__) . '/../../include/DbConnect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }

    public function search($params){

    	$sql = "SELECT * FROM inventory WHERE cid = :cid and ( vin LIKE :search OR make LIKE :search OR model LIKE :search )";
    	$stmt = $this->conn->prepare($sql);
		$stmt->bindParam(":cid", $params["cid"]);
		$stmt->bindParam(":search", $params["search"]);
        $stmt = $this->conn->prepare($sql);

        $result = $stmt->execute();            
        // Check for successful insertion
        return $stmt->fetchAll(PDO::FETCH_ASSOC);	

    }

    public function advanceSearch($search){

    	$stmt = $this->conn->prepare('SELECT * FROM advance_search WHERE cid = :cid and ( searchOn LIKE :search )');
		$stmt->bindParam(":search", $vinNumber);
        $stmt = $this->conn->prepare($sql);
        // $stmt->bindParam(":vin", $vinNumber);
        $result = $stmt->execute();            
        // Check for successful insertion
        return $stmt->fetchAll(PDO::FETCH_ASSOC);	

    }    


}

?>