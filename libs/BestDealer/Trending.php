<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Ravi Tamada
 * @link URL Tutorial link
 */
class Trending {

    public static $conn;

    public static function init() {
        require_once dirname(__FILE__) . '/../../include/DbConnect.php';
        // opening db connection
        $db = new DbConnect();
        self::$conn = $db->connect();
    }


    /**
     * Creating new user
     * @param String $name User full name
     * @param String $email User login email id
     * @param String $password User login password
     */
    public static function getTrending() {
		
		self::init();	
		
		$sql = " select * from trending limit 0 , 5 " ; 
		// insert query
		$stmt = self::$conn->prepare($sql);
		// $stmt->bindParam(":vin", $vinNumber);
		$result = $stmt->execute();            
		// Check for successful insertion
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
	


}


?>