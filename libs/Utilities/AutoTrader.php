<?php
// Yii extension created by Marco Troisi
// Based on Simon Willison's php XMLWriter class
// Based on Lars Marius Garshol's Python XMLWriter class
// See http://www.xml.com/pub/a/2003/04/09/py-xml.html
// For any question: hello@marcotroisi.com
/* http://www.autotrader.com/cars-for-sale/2013/Acura/TL/Lilburn+GA-30047?
endYear=2013&
makeCode1=ACURA&
modelCode1=TL&
searchRadius=25&
showcaseListingId=0&showcaseOwnerId=0&sortBy=bestmatchb&
startYear=2013&
Log=0
*/

class AutoTrader
{
	private $url = 'http://www.autotrader.com/cars-for-sale/';
	private $app_id;
	private $version = '1.0.0';
	private $global_id;
	private $format = 'JSON';
	private $styleID;
	private $errors;
	

	public function __construct($params = null){
					
		if(!is_null($params))
			$this->params = $params;
		else
			$errors[] = 'Style ID required';		
	}



	public function getVehicles($distance = 200 ){
			
		$results["success"] = FALSE;
		$results["message"] = '';
		$results["data"]	= NULL;
							
		$url 	= $this->url . '/' . $this->params["yr"]. '/' . $this->params["make"]. '/' . $this->params["model"]. '/' . $this->params["zipComplete"].'?';
		$url .= 'endYear=' . $this->params["yr"] ;
		$url .= '&makeCode1=' . $this->params["make"] ;
		$url .= '&modelCode1=' . $this->params["model"] ;
		$url .= '&searchRadius=' . $distance ;
		$url .= '&showcaseListingId=0&showcaseOwnerId=0&sortBy=bestmatchb' ;		
		$url .= '&startYear=' . $this->params["yr"] ;
		$url .= '&Log=0' ;							
		
		$rs 			 = processURL($url , 'AUTO' , GetYiiParams("style").'/'.$this->params["styleID"].'/AutoTrader_'. $this->params["zipComplete"].'_'.date("Ymd") , $this->params["styleID"] );

		if(isset($rs["data"]) && !empty($rs["data"])){
		
			$dom = new DOMDocument();
			@$dom->loadHTML($rs["data"]);

			$xpath = new DomXPath($dom);
			$count  = 0;
			$final  = array();
			// step 1 - links:
			$aLinks = array();
			$items = $xpath->query("//div[starts-with(@class, 'listing listing-findcar')]");
			foreach ($items as $item ) {
				
				$htmlString = $dom->saveHTML($item); 
				if(!empty($htmlString)){
				
					
					$miles = $distance = $prices = array();
					
					preg_match_all('/<span class="atcui-bold">([0-9\,]+)<\/span>/', $htmlString , $miles);
					preg_match_all('|<span data-birf-ref="../../">(.*?)</span>|', $htmlString , $prices);
					preg_match_all('|<span class="distance-cont">(.*?)</span>|', $htmlString , $distance);
					
					// echo $htmlString;								
					if(isset($prices[1][0]) && !empty($prices[1][0]) ){
											
						$price         = str_replace( array(",","$") , "" , $prices[1][0] ); 	

						if( (int)$price > 0 ){
						
							$final[$count] 				= array( "price"=>0 , "mileage"=>0 , "distance"=> 0 );
							$final[$count]['price']		= (int)$price; 
						
							if(isset($miles[1][0]) && !empty($miles[1][0]) ){
								$final[$count]['mileage']		    = str_replace( "," , "" , $miles[1][0] );
							}	
							
							if(isset($distance[1][0]) && !empty($distance[1][0]) ){
								$miles1 					= trim($distance[1][0]);
								$dist 						= explode(" ", $miles1 );
								$final[$count]['distance']	= $dist[0] ;
							}						
							$count++;

						}
						
					}						
												
					
				}

			}

		}	
			
		$results["data"] 	= json_decode( json_encode($final) , true);
		$results["success"] = is_null($results["data"])?FALSE:TRUE;	
		return $results;		
		

	}

	
}


?>  
