<?php
// Yii extension created by Marco Troisi
// Based on Simon Willison's php XMLWriter class
// Based on Lars Marius Garshol's Python XMLWriter class
// See http://www.xml.com/pub/a/2003/04/09/py-xml.html
// For any question: hello@marcotroisi.com

class CraigsList {

		var $file;
		var $styleID;
		var $fileName;
		
		function  __construct($styleID=null) 
		{ 	
			if(!is_null($styleID))
				$this->styleID = $styleID;
			else
				die('Style ID required');
			
			$this->fileName = 'CL_Search_'.date("Ymd");			
			$this->file 	= GetYiiParams("style").$this->styleID.'/'.$this->fileName.".txt";	
			
			
		} 			
	
		public function  Search($params){

			$results["success"] = false;
			$results["message"] = '';
			$results["data"]	= null;
			$craigslistURL      = null;
			
			
			
			if(!isset($params["url"])){				
				$cl 			= findCraigslist(trim($params["zipcode"]));			
				$craigslistURL  = str_replace("http" , "https" , $cl->craigslist);	
			}
			else
			    $craigslistURL  	= $params["url"]; 
			

			$query 		 = $params["make"] . " " . $params["model"];
			$query		 = urlencode($query);					
			
			// get owners
			$url    	 = "{$craigslistURL}search/cto?query={$query}";	
			$url		.= "&autoMinYear=".$params["yr"]."&autoMaxYear=".$params["yr"];
			$url		.= "&srchType=T";
			$url		.= "&format=rss";			
			
			$rs 			  = processURL($url , 'CL' , GetYiiParams("style").'/'.$this->styleID.'/CL_owner_findItems'.date("Ymd").'.xml' , $this->styleID   );	
			$simpleXml 		  = simplexml_load_string(fix_latin1_mangled_with_utf8_maybe_hopefully_most_of_the_time($rs["data"]));
			$results1["data"] = json_decode( json_encode($simpleXml)  , true );	
						
			// get dealers
			$url    	 = "{$craigslistURL}search/cta?query={$query}";	
			$url		.= "&autoMinYear=".$params["yr"]."&autoMaxYear=".$params["yr"];
			$url		.= "&srchType=T";
			$url		.= "&format=rss";			
			
			$rs 			 = processURL($url , 'CL' , GetYiiParams("style").'/'.$this->styleID.'/CL_dealer_findItems'.date("Ymd").'.xml' , $this->styleID );	
			$simpleXml 		 = simplexml_load_string(fix_latin1_mangled_with_utf8_maybe_hopefully_most_of_the_time($rs["data"]));
			$results2["data"] = json_decode( json_encode($simpleXml) , true );					
			
			$rs1 = $rs2 = array();
			
			if(isset($results1["data"]["item"]))
				$rs1 			= $this->GetData($results1["data"]["item"]);
				
			if(isset($results2["data"]["item"]))
				$rs2 			= $this->GetData($results2["data"]["item"]);	
			
			return	 array_merge($rs1, $rs2);
			
		}
		
		/* Loop through an ITEM array to get all prices */
		public function GetData($items = array() ) 
		{

			$results["success"] = FALSE;
			$results["message"] = '';
			$results["data"]	= NULL;
		
			if(!empty($items)){

				$count = 0;
				$final = array();
				
				foreach($items as $item){
					
					$url 						= '';
					
					if(isset($item["link"]) && !empty($item["link"]))
						$url  						= $item["link"];
					
					if(!empty($url)){
					
						$path 						= parse_url($url);
						$fileName 					= basename($path["path"]);					
										
						$rs 			 			= processURL($url , 'CL' , GetYiiParams("style").$this->styleID.'/CL_'.$fileName );	
						// $xml 		 				= simplexml_load_string(fix_latin1_mangled_with_utf8_maybe_hopefully_most_of_the_time($rs["data"]));
						//$xml 			 			= simplexml_load_string( $rs["data"] , null , LIBXML_NOCDATA );
						//$xmlToJSON 		 			= json_encode($xml);
						//$html 						= json_decode($xmlToJSON,TRUE);	
						$html						 = NULL;	
						$html						 = $rs["data"];
						
						if(isset($html) ){
														
							$h2 					= preg_match_all('/<h2 class=\"postingtitle\">(.*?)<\/h2>/s', $html, $h2tags);
							$price 					= preg_match_all('/&#x0024;(\d*)/', $h2tags[0][0], $prices);
							$miles					= preg_match_all('/<span>odometer: <b>(.*?)<\/b>/', $html, $mile);
							
							preg_match_all('/datetime\=\"(\d{4})(-)(\d{2})(-)(\d{2})(T)(\d{2})(:)(\d{2})(:)(\d{2})(-)(\d{4})/' , $html , $date);
							
							//$final[$count]['html']	= $html;  // Outputs the HTML code
							
							if(isset($prices[1][0]) && (int)$prices[1][0] > 0){
								
								$final[$count] = array( "price"=>0 , "mileage"=>0 , "distance"=> 0 );
								
								$final[$count]['price']	= (int)$prices[1][0];	
								
								if(isset($mile[1][0]))
									$final[$count]['mileage']	= $mile[1][0];
								
								if(isset($date[1][0]))
									$final[$count]['date']	= strtotime(str_replace( 'datetime="' ,"" , $date[0][1] ));

								$count++;	
							}
						
						}
					
					}
				
				}
				
				$results['message'] = $count . " Records Found.";
				$results['data'] = $final;				

			}
			
			$results["success"] = is_null($results["data"])?FALSE:TRUE;								
			return $results;			
		
		}
		
			
    
}
?>  
