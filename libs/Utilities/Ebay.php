<?php 

class Ebay
{
	private $url = 'http://svcs.ebay.com/services/search/FindingService/v1';
	private $itemUrl = 'http://open.api.ebay.com/shopping';
	private $app_id;
	private $version = '1.0.0';
	private $global_id;
	private $format = 'JSON';
	private $styleID;
	

	public function __construct($styleID = null){
		
		$config				= GetYiiParams("ebayPROD");		
		$this->app_id 		= $config["AppID"]; 
		$this->global_id 	= "EBAY-US"; 	
			
		if(!is_null($styleID))
			$this->styleID = $styleID;
		else
			die('EBAY: Style ID required');		
	}

	public function findItems($keyword = '', $limit = 200 ){
			
		$results["success"] = FALSE;
		$results["message"] = '';
		$results["data"]	= NULL;
							
		$url 	= $this->url . '?';
		$url .= 'operation-name=findItemsByKeywords';
		$url .= '&service-version=' . $this->version;
		$url .= '&keywords=' . urlencode($keyword);
		$url .= '&itemFilter(0).name=ListingType';
		$url .= '&itemFilter(0).value=FixedPrice';		
		$url .= '&paginationInput.entriesPerPage=' . $limit;		
		$url .= '&security-appname='. $this->app_id;
		$url .= '&response-data-format=' . $this->format;							
		
		$rs = processURL($url , 'EBAY' , GetYiiParams("style").'/'.$this->styleID.'/Ebay_findItems'.date("Ymd") );
		
		foreach($rs->findItemsByKeywordsResponse[0]->searchResult[0]->item as $item)
				$items[$item->itemId[0]] = $item->itemId[0];
				
		$this->getItemSpecifics($items);		
		
	}
	
	
	public function findItemsAdvanced($keyword = '', $item_sort = 'BestMatch', $item_type = 'FixedPricedItem', $min_price = '0', $max_price = '9999999', $limit = 200){

		$results["success"] = FALSE;
		$results["message"] = '';
		$results["data"]	= NULL;	
	
		$url 	=  $this->url . '?';
		$url .= 'operation-name=findItemsAdvanced';
		$url .= '&service-version=' . $this->version;
		$url .= '&global-id=' . $this->global_id;
		$url .= '&keywords=' . urlencode($keyword);
		$url  .= "&categoryId=6001";
		$url .= '&sortOrder=BestMatch';
		$url .= '&itemFilter(0).name=ListingType';
		$url .= '&itemFilter(0).value=FixedPrice';
		$url .= '&itemFilter(1).name=MinPrice';
		$url .= '&itemFilter(1).value=' . $min_price;
		$url .= '&itemFilter(2).name=MaxPrice';
		$url .= '&itemFilter(2).value=' . $max_price;
		$url .= '&paginationInput.entriesPerPage=' . $limit;
		$url .= '&descriptionSearch=false';
		$url .= '&security-appname='. $this->app_id;
		$url .= '&response-data-format=' . $this->format;
		
		$rs 		= processURL($url , 'EBAY' , GetYiiParams("style").'/'.$this->styleID.'/Ebay_findItemsAdvanced'.date("Ymd") , $this->styleID   );		
		$items 		= null;
		
		$records 	=  json_decode( $rs["data"]   ) ;
			
		
		if(isset($records->findItemsAdvancedResponse)){
			
			foreach($records->findItemsAdvancedResponse[0]->searchResult[0]->item as $item){
					
					if(isset($item->postalCode[0]) && !empty($item->postalCode[0]))
						$items[$item->itemId[0]]["zip"] = $item->postalCode[0];
						
					if(isset($item->attribute[1]->value[0]) && !empty($item->attribute[1]->value[0]))	
						$items[$item->itemId[0]]["mileage"] = $item->attribute[1]->value[0];
						
					if(isset($item->sellingStatus[0]->convertedCurrentPrice[0]->__value__) && !empty($item->sellingStatus[0]->convertedCurrentPrice[0]->__value__))
						$items[$item->itemId[0]]["price"] = $item->sellingStatus[0]->convertedCurrentPrice[0]->__value__;
			}		
		
			$results["success"] = is_null($items)?FALSE:TRUE;
			$results["data"] 	= $items;		
		
		}
		
		return $results;
		
	}

	public function getItemSpecifics($anArrayOfItems)
	{
		
		$results["success"] = FALSE;
		$results["message"] = '';
		$results["data"]	= NULL;		
		
		$results = array_chunk($anArrayOfItems , 20 , true );
		
		foreach($results as $itemsIds ){
		
				$url 	= $this->itemUrl . '?';
				$url .= 'callname=GetMultipleItems';
				$url .= '&responseencoding=' . $this->format;
				$url .= '&appid=' . $this->app_id;	
				$url .= '&siteid=0&version=525&IncludeSelector=ItemSpecifics' ;		
				$url .= '&ItemID='. implode("," , $itemsIds );				
		
				$rs = processURL($url , $api , $fileName , $this->styleID  );

								
				// $results["data"] 	= json_decode($xml , true);
				
		}	
		
		
		
		$results["success"] = is_null($results["data"])?FALSE:TRUE;	
		return $results;	
	
	}	
	
	public function sortOrders(){
		$sort_orders = array(
			'BestMatch' => 'Best Match',
			'BidCountFewest' => 'Bid Count Fewest',
			'BidCountMost' => 'Bid Count Most',
			'CountryAscending' => 'Country Ascending',
			'CountryDescending' => 'Country Descending',
			'CurrentPriceHighest' => 'Current Highest Price',
			'DistanceNearest' => 'Nearest Distance',
			'EndTimeSoonest' => 'End Time Soonest',
			'PricePlusShippingHighest' => 'Price Plus Shipping Highest',
			'PricePlusShippingLowest' => 'Price Plus Shipping Lowest',
			'StartTimeNewest' => 'Start Time Newest'
		);

		return $sort_orders;
	}
}
?>