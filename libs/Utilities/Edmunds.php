<?php
// Yii extension created by Marco Troisi
// Based on Simon Willison's php XMLWriter class
// Based on Lars Marius Garshol's Python XMLWriter class
// See http://www.xml.com/pub/a/2003/04/09/py-xml.html
// For any question: hello@marcotroisi.com

class Edmunds {

		var $apiKey;
		
		function  __construct() 
		{ 	
			$this->apiKey = GetYiiParams('edmundsAPI'); 
			
			if(empty($this->apiKey))
				die('API Needed');
				
			$this->baseFolder = GetYiiParams('data');	
		} 	
	
		public function  GetVehicleRecallInfo($ModelYearID){

				 if (file_exists("{$this->baseFolder}/recall/".$ModelYearID)) {
					$jsondata 	= file_get_contents( "{$this->baseFolder}/recall/".$ModelYearID , true);
					$jsondata   = json_decode($jsondata , true);					
										
				}
				else{
					$jsondata 	= false;
					$url 		= "https://api.edmunds.com/v1/api/maintenance/recallrepository/findbymodelyearid?modelyearid={$ModelYearID}&fmt=json&api_key={$this->apiKey}";
					$jsondata 	= file_get_contents( $url , true);	
					
					file_put_contents( "{$this->baseFolder}/recall/". $ModelYearID, $jsondata);
					
					$jsondata = json_decode($jsondata , true);
					
				}
				
				return $jsondata;		
		
		
		}
		
		public function  GetVehiclePhoto($styleID){


				 if (file_exists("{$this->baseFolder}/img/".$styleID)) {
					$jsondata 	= file_get_contents( "{$this->baseFolder}/img/".$styleID , true);
					$jsondata   = json_decode($jsondata , true);					
										
				}
				else{
					$jsondata 	= false;
					$url 		= "https://api.edmunds.com/v1/api/vehiclephoto/service/findphotosbystyleid?styleId={$styleID}&fmt=json&api_key={$this->apiKey}";
					$jsondata 	= file_get_contents( $url , true);	
					
					file_put_contents( "{$this->baseFolder}/img/". $styleID, $jsondata);
					
					$jsondata = json_decode($jsondata , true);
					
				}
				
				return $jsondata;
			
		}


		public function  GetAutoValueByModelMakeYear($styleID){
				
				if(isset($_SESSION['styleID'][$styleID])){
					$jsondata = json_decode($_COOKIE['photo_'.$styleID]); 				
				}
				else{
					$_SESSION['styleID'][$styleID] = $styleID;
					$jsondata 	= false;
					$url 		= "https://api.edmunds.com/v1/api/vehiclephoto/service/findphotosbystyleid?styleId={$styleID}&fmt=json&api_key={$this->apiKey}";
					$jsondata 	= file_get_contents( $url , true);	
					setcookie( 'photo_'.$styleID , $jsondata ); 					
				}
				return $jsondata;
		
		}	

		
		public function  GetCertifiedVehicleCost($styleID , $zipCode){
				
				$jsondata 	= false;
				$url 		= "https://api.edmunds.com/v1/api/tmv/tmvservice/findcertifiedpriceforstyle?styleid={$styleID}&zip={$zipCode}&fmt=json&api_key={$this->apiKey}";
				$jsondata 	= file_get_contents( $url , true);
				return $jsondata;
		
		}

		public function  GetUsedVehicleResaleValue($styleID , $mileage , $zipCode){

				 $hash = md5($styleID.$mileage.$zipCode);
		
		
				 if (file_exists("{$this->baseFolder}/cost/".$hash)) {
					$jsondata 	= file_get_contents( "{$this->baseFolder}/cost/".$hash , true);
					$jsondata   = json_decode($jsondata , true);					
										
				}
				else{
				
					$url 				= "https://api.edmunds.com/v1/api/tmv/tmvservice/calculateusedtmv?styleid={$styleID}&condition=Outstanding&mileage={$mileage}&zip={$zipCode}&fmt=json&api_key={$this->apiKey}";
					$jsondata["Outstanding"] 	= file_get_contents( $url , true);						
					
					$url 				= "https://api.edmunds.com/v1/api/tmv/tmvservice/calculateusedtmv?styleid={$styleID}&condition=Clean&mileage={$mileage}&zip={$zipCode}&fmt=json&api_key={$this->apiKey}";
					$jsondata["Clean"] 		= file_get_contents( $url , true);	
					
					sleep(5);
					
					$url 				= "https://api.edmunds.com/v1/api/tmv/tmvservice/calculateusedtmv?styleid={$styleID}&condition=Average&mileage={$mileage}&zip={$zipCode}&fmt=json&api_key={$this->apiKey}";
					$jsondata["Average"] 		= file_get_contents( $url , true);	
					

					$url 				= "https://api.edmunds.com/v1/api/tmv/tmvservice/calculateusedtmv?styleid={$styleID}&condition=Rough&mileage={$mileage}&zip={$zipCode}&fmt=json&api_key={$this->apiKey}";
					$jsondata["Rough"] 		= file_get_contents( $url , true);	
					
					sleep(5);
					
					$url 				= "https://api.edmunds.com/v1/api/tmv/tmvservice/calculateusedtmv?styleid={$styleID}&condition=Damaged&mileage={$mileage}&zip={$zipCode}&fmt=json&api_key={$this->apiKey}";
					$jsondata["Damaged"] 		= file_get_contents( $url , true);
					
					
					file_put_contents( "{$this->baseFolder}/cost/". $hash, json_encode($jsondata) );
					
				}		

				return $jsondata;				
		
		}			
	
	
		public function  findCarByVIN($theVIN) {		
			
			$results["success"] = FALSE;
			$results["message"] = '';
			$results["data"]	= NULL;
			
			$url 		= "https://api.edmunds.com/api/vehicle/v2/vins/{$theVIN}?fmt=json&api_key={$this->apiKey}";						
			
			$this->baseFolder.'vin/'.$theVIN ;
			
			$results = processURL($url , 'EDMUNDS' , $this->baseFolder.'vin/'.$theVIN  );
		
			
			$results["success"] = empty($results["data"])?FALSE:TRUE;	
			return $results;
		
		}	
    
}
?>  
