<?php





function processURL($url , $api , $fileName , $styleID ='' , $dontAppendToFile = true ){
        
        global $db;

        $results["success"] = FALSE;
        $results["message"] = '';
        $results["data"]    = NULL;     
                        
		      
        //
        //  Check to determine if query already existed
        //
        $sql = "SELECT * FROM cron_api_queries WHERE api = :api AND url = :url";
        $sth = $db->con->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute(array(':api' => $api , ':url' => $url ));
        $results = $sth->fetchAll();

        //
        // Checks if query was executed 
        //  and if it has retrieve data from file.
        //

        if(count($results) > 0 ){

            $contents = file_get_contents($fileName);  
            if(!empty($contents)){
                $results["data"]    = $contents  ;            
            }
            else                
                $results["message"] = 'File not found :'. $fileName ;

        } else {               
        
                //
                //  New query will check if fle exists
                //
                if( is_null($results["data"]) ){

                    if (!empty($styleID) && !file_exists(GetYiiParams("style").'/'.$styleID)) {                         
                        if (!mkdir(GetYiiParams("style").'/'.$styleID, 0777, true)) {
                            return $results;
                        }   
                        else
                            $results["message"][] =  "Loaded from file." . $fileName ;              
                    }           
                    
                                        
                    $xml    = file_get_contents($url);          
                    $sql    = "INSERT INTO cron_api_queries (api,url) VALUES (:api,:url) ";
                    $sth    = $db->con->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                                     
                    
                    if( $sth->execute(array(':api' => $api , ':url' => $url )) == FALSE ){
                        $results["message"][] = implode_r( "" , $search->getErrors() );;
                    }
                    else {
                        
                        if(!empty($xml)){ 
                            // print_r($xml);
                            if($dontAppendToFile)       
                                $cfile   = file_put_contents($fileName , $xml );
                            else
                                $cfile   = file_put_contents($fileName , $xml , FILE_APPEND | LOCK_EX );
                            
                            if($cfile == false){
                                $results["message"] = 'File not created -- ' . $fileName;
                            } else{ 
                                $results["message"] = 'Didnt write to file query';  
                            }
                            
                            
                            $results["data"]    = $xml;
                            
                        }   
                        else
                            $results["message"] = 'No data found';  
                    }                       
                    
                }

        }

        $results["success"] = is_null($results["data"])?FALSE:TRUE; 
        return $results;
        
    }       


?>