<?php

	///
	///  takes the files which USERS have search for and extracts the information and store in DB
	///
	///

	function  getIPAddress() {

		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} elseif (!empty($_SERVER['REMOTE_ADDR'])) {
			$ip = $_SERVER['REMOTE_ADDR'];
		} else {
			$ip = '127.0.0.1';
		}
		
			return $ip;
	}
	
	
	require_once '../include/DbConnect.php';
	require_once '../libs/BestDealer/Processes.php';
	require_once '../libs/BestDealer/Inventory.php';
	require_once '../libs/BestDealer/CraigsList.php';

	
	$conn = new DbConnect();

	$conn->connect();

	
	$inv = new Inventory();
	$auto = $inv->getVehicleByStyleID(100633142);
	
	
	$params["make"]    = $auto[0]["make"];
	$params["model"]   = $auto[0]["model"];
	$params["yr"]      = $auto[0]["year"];
	$params["zipcode"] = "30047";
	$params["styleID"] = $auto[0]["styleID"];
	
     $cl 					= CraigsList::init($params["styleID"]);
	 $rsCraigsList			= CraigsList::Search($params);	
	
	  
	  file_put_contents( $params["styleID"].".txt"  , json_encode($rsCraigsList) );
	
	/*

	function placeholders($text, $count=0, $separator=","){
	    $result = array();
	    if($count > 0){
	        for($x=0; $x<$count; $x++){
	            $result[] = $text;
	        }
	    }

	    return implode($separator, $result);
	}


	if ($handle = opendir(DATA_FOLDER . date('Ymd') )) {
	    while (false !== ($files = readdir($handle))) {
	        if ($files != "." && $files != "..") {
	            $filename[] = $files;
	        }
	    }
	    closedir($handle);
	}

	

	foreach($filename as $file){
			
			$string  = file_get_contents($file);
			$results = json_decode($string, true);
			
			foreach($results as $item){
					$datafields = array_keys(item);
					$data[] = array_values(item);
			}
	}

	$conn->beginTransaction(); // also helps speed up your inserts.
	
	$insert_values = array();
	foreach($data as $d){
	    $question_marks[] = '('  . placeholders('?', sizeof($d)) . ')';
	    $insert_values 	  = array_merge($insert_values, array_values($d));
	}

	$sql = "INSERT INTO pricing_research (" . implode(",", array_keys($datafields) ) . ") VALUES " . implode(',', $question_marks);

	$stmt = $conn->prepare ($sql);
	try {
	    $stmt->execute($insert_values);
	} catch (PDOException $e){
	    echo $e->getMessage();
	}

	$conn->commit();

	*/

?>