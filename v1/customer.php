<?php

require_once '../include/DbHandler.php';
require_once '../include/PassHash.php';
require '../libs/Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();
$app->contentType('application/json');

// User id from db - Global Variable
$user_id = NULL;
$access  = NULL;

/**
 * Adding Middle Layer to authenticate every request
 * Checking if the request has valid api key in the 'Authorization' header
 */
function authenticate(\Slim\Route $route) {
    // Getting request headers
    $headers  = apache_request_headers();
    $response = array();
    $app      = \Slim\Slim::getInstance();

    // Verifying Authorization Header
    if (isset($headers['authorization'])) {
        $db = new DbHandler();

        // get the api key
        $api_key = $headers['authorization'];

        // validating api key
        if ( $db->isValidApiKey($api_key) === false ) {
            // api key is not present in users table
            $response["error"] = true;
            $response["message"] = "Access Denied. Invalid Api key";
            echoRespnse(401, $response);
            $app->stop();
        } else {
            global $user_id;
			
			global $access;
            // get user primary key id
            $user    = $db->getUserId($api_key);
			$user_id = $user["userid"];

        }
    } else {
        // api key is missing in header
        $response["error"] = true;
        $response["message"] = "Api key is misssing" . implode("," , $headers );
        echoRespnse(400, $response);
        $app->stop();
    }
}

/**
 * ----------- METHODS WITHOUT AUTHENTICATION ---------------------------------
 */

/**
 * ----------- METHODS WITHOUT AUTHENTICATION ---------------------------------
 */
 
 
 /**
 * User Registration
 * url - /register
 * method - POST
 * params - name, email, password
 */

 // GET route
$app->get(
    '/',
    function () {
        $template = <<<EOT
            Welcome to BESTDEALER API
EOT;
        echoRespnse(201, $template); 
        //echo $template;
    }
);

 


/**
 * User Registration
 * url - /register
 * method - POST
 * params - name, email, password
 */
$app->get('/register', function() use ($app) {
            // check for required params
            verifyRequiredParams(array( 'email', 'password'));

            $response = array();

            $params = null;
            $params    = json_decode($app->request->getBody() , true );

            echo $app->request->getBody();
            print_r($params);
            die();
            // validating email address
            validateEmail($params["email"]);

            $db = new DbHandler();
            $db->beginTransaction();
            $res = $db->createUser($params);

            if ($res == USER_CREATED_SUCCESSFULLY) {
                
                require_once dirname(__FILE__) . '/../libs/stripe/init.php';    
                \Stripe\Stripe::setApiKey('sk_test_T8dTq7IAFkkWt6tscB3dcEKE');
                $result = \Stripe\Customer::create(array(
                    "email" => $params["email"],
                    "description" => "Mobile Customer",
                    //"coupon" => (isset($params["coupon"]))?$params["coupon"]:"none",
                    // "trial_end" =>  strtotime("+1 month") 
                    // obtained with Stripe.js
                )); 
                $array = array( "email" => $params["email"] , "customerid" => $result["id"]   );
                $db->updateUser($array);    
                $db->commit();        
                $response["error"] = false;
                $response["message"] = "You are successfully registered";
            } else if ($res == USER_CREATE_FAILED) {
                $db->rollback();
                $response["error"] = true;
                $response["message"] = "Oops! An error occurred while registereing";
            } else if ($res == USER_ALREADY_EXISTED) {
                $db->rollback();
                $response["error"] = true;
                $response["message"] = "Sorry, this email already existed";
            }
            // echo json response
            echoRespnse(200, $response);
        });

	

/**
 * Listing all tasks of particual user
 * method GET
 * url /tasks          
 */
$app->get('/car/makeByYear', 'authenticate', function() use ($app) {
                        
                        
            verifyRequiredParams(array( 'year'));            
                        
            $year    =  $app->request->params('year');

            $db = new DbConnect();
            $conn = $db->connect();

            $sql = " SELECT DISTINCT make FROM `make_model` where year = " . $year  ; 
                // insert query
            $stmt = $conn->prepare($sql);
                // $stmt->bindParam(":vin", $vinNumber);
            $result = $stmt->execute();            
                // Check for successful insertion
            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);  
         
            
            echoRespnse(200, $rows);
        }); 
        
/**
 * Listing all tasks of particual user
 * method GET
 * url /tasks          
 */
$app->get('/car/model(/:year)(/:make)', 'authenticate', function( $year , $make ) use ($app) {
                        
            $db = new DbConnect();
            $conn = $db->connect();

            $sql = " SELECT distinct model FROM `make_model` where year = ".$year."  and make = '".$make."' " ; 
                // insert query
            $stmt = $conn->prepare($sql);
                // $stmt->bindParam(":vin", $vinNumber);
            $result = $stmt->execute();            
                // Check for successful insertion
            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);  
         
            echoRespnse(200, $rows);
        });        
		
/**
 * User Login
 * url - /login
 * method - POST
 * params - email, password
 */
$app->post('/login', function() use ($app) {
            // check for required params
			verifyRequiredParams(array('email', 'password'));
			
			$params    = json_decode($app->request->getBody() , true );
			$email     = $params['email'];
			$password  = $params['password'];			
            $response  = array();

            $db = new DbHandler();
            // check for correct email and password
			
            if ($db->checkLogin($email, $password)) {
                // get the user by email
                $user = $db->getUserByEmail($email);

                if ($user != NULL) {
                    $response["error"] = false;
                    $response['email'] = $user['email'];
                    $response['apiKey'] = $user['api_key'];
                    $response['created'] = $user['created'];
                    $db->lastVisited($user["userid"]);

                } else {
                    // unknown error occurred
                    $response['error'] = true;
                    $response['message'] = "An error occurred. Please try again";
                }
            } else {
                // user credentials are wrong
                $response['error'] = true;
                $response['message'] = 'Login failed. Incorrect credentials';
            }

            echoRespnse(200, $response);
        });


$app->get('/reset', function() use ($app) {
            // check for required params
            verifyRequiredParams(array('email'));
            
            $params    = json_decode($app->request->getBody() , true );
            $email     = $params['email'];          
            $response  = array();

            $db = new DbHandler();


            $response["error"] = false;
            $response['message'] = 'Password reset';

            echoRespnse(200, $response);
        });


$app->get('/trending', 'authenticate' , function() use ($app) {
                        
                         
                $db = new DbConnect();
                $conn = $db->connect();

                $sql = " select * from rand_inventory   limit 0 , 5 " ; 
                // insert query
                $stmt = $conn->prepare($sql);
                // $stmt->bindParam(":vin", $vinNumber);
                $result = $stmt->execute();            
                // Check for successful insertion

                $response 				 = array();  
				$response["error"]		 = true;
				$response["data"] 		 = $stmt->fetchAll(PDO::FETCH_ASSOC);		
                         
                echoRespnse(200, $response);
        });				
		
		
$app->get('/user/edit', 'authenticate', function() use ($app) {
                        
            require_once dirname(__FILE__) . '/../libs/BestDealer/Dealer.php';
            
            global $user_id;
            
            $response   = array();            
            $db 	    = new User();
            $params 	= null;
            $params     = json_decode($app->request->getBody() , true );
            
            

            $results = $db->manageCompany($params);
            $response["error"] = true;
            $response["data"] = $results;           
            
            echoRespnse(200, $response);
        }); 		


$app->get('/company/manage', 'authenticate', function() use ($app) {
                        
            require_once dirname(__FILE__) . '/../libs/BestDealer/Dealer.php';
            
            global $user_id;
            
            $response  = array();            
            $db        = new Company();
            $params    = null;
            $params    = json_decode($app->request->getBody() , true );
            
            

            $results = $db->manageCompany($params);
            $response["error"] = true;
            $response["data"] = $results;           
            
            echoRespnse(200, $response);
        }); 
		
 

 /*
 * ------------------------ DEALERSHIP Functions  ------------------------
 */


/**
 * Listing all tasks of particual user
 * method GET
 * url /tasks          
 */
$app->post('/dealership/register',  function() use ($app) {
                        
            // check for required params
            verifyRequiredParams(array( 'email', 'password'));

            $response = array();

            $params["email"] =  $app->request->params('email');
            $params["password"] =  $app->request->params('password');
            // validating email address
            validateEmail($params["email"]);

            $db = new DbHandler();
            $db->beginTransaction();
            $res = $db->createUser($params);

            if ($res == USER_CREATED_SUCCESSFULLY) {
                
                require_once dirname(__FILE__) . '/../libs/stripe/init.php';    
                \Stripe\Stripe::setApiKey('sk_test_T8dTq7IAFkkWt6tscB3dcEKE');
                $result = \Stripe\Customer::create(array(
                    "email" => $params["email"],
                    "description" => "Mobile Customer",
                    //"coupon" => (isset($params["coupon"]))?$params["coupon"]:"none",
                    // "trial_end" =>  strtotime("+1 month") 
                    // obtained with Stripe.js
                )); 
                $array = array( "email" => $params["email"] , "customerid" => $result["id"]   );
                $db->updateUser($array);    
                $db->commit();        
                $response["error"] = false;
                $response["message"] = "You are successfully registered";
            } else if ($res == USER_CREATE_FAILED) {
                $db->rollback();
                $response["error"] = true;
                $response["message"] = "Oops! An error occurred while registereing";
            } else if ($res == USER_ALREADY_EXISTED) {
                $db->rollback();
                $response["error"] = true;
                $response["message"] = "Sorry, this email already existed";
            }
            // echo json response
            echoRespnse(200, $response);
        });
        

/**
 * Listing all tasks of particual user
 * method GET
 * url /tasks          
 */
$app->get('/dealership/forgot', function() use ($app) {
                        
            require_once dirname(__FILE__) . '/../libs/BestDealer/Dealer.php';
            
            verifyRequiredParams(array( 'email'));
            $params["email"] =  $app->request->params('email');       
            validateEmail($params["email"]);
            

            $response["error"] = false;
            $response["data"] = $results;           
            
            echoRespnse(200, $response);
        });         


/**
 * Listing all tasks of particual user
 * method GET
 * url /tasks          
 */
$app->post('/dealership/login', function() use ($app) {
                        
            // check for required params
			verifyRequiredParams(array('email', 'password'));
			
            $response  = array();
            $email =  $app->request->params('email');
            $password =  $app->request->params('password');            

            $db = new DbHandler();
            // check for correct email and password
			
            if ($db->checkLogin($email, $password)) {
                // get the user by email
                $user = $db->getUserByEmail($email);

                if ($user != NULL) {
                    global $user_id;
                    $user_id = $user["userid"];
                    $response["error"] = false;
                    $response['email'] = $user['email'];
                    $response['apiKey'] = $user['api_key'];
                    $response['created'] = $user['created'];
                    $db->lastVisited($user["userid"]);

                } else {
                    // unknown error occurred
                    $response['error'] = true;
                    $response['message'] = "An error occurred. Please try again";
                }
            } else {
                // user credentials are wrong
                $response['error'] = true;
                $response['message'] = 'Login failed. Incorrect credentials';
            }

            echoRespnse(200, $response);
        }); 


/**
 * Listing all tasks of particual user
 * method GET
 * url /tasks          
 
$app->get('/dealership/manage', 'authenticate', function() use ($app) {
                        
            require_once dirname(__FILE__) . '/../libs/BestDealer/Dealer.php';
            
            global $user_id;
            
            $response = array();            
            $db = new Dealer();
            $params = null;
            $params    = json_decode($app->request->getBody() , true );
            
            

            $results = $db->manageCompany($params);
            $response["error"] = true;
            $response["data"] = $results;           
            
            echoRespnse(200, $response);
        }); 
     */   

$app->get('/dealership/trending', 'authenticate', function() use ($app) {
                        
            global $user_id;
            
            $response = array();   
            $response= [ $user_id  ];

            echoRespnse(200, $response);
 });
 
$app->get('/dealership/reports', 'authenticate', function() use ($app) {
                    
            require_once dirname(__FILE__) . '/../libs/Reports/init.php';
            
            global $user_id;
            			
            $response  = array();            
            $reports   = new Reports();
			$response  = $reports->getReports($cid);
            echoRespnse(200, $response);
 }); 
        
/**
 * Listing all tasks of particual user
 * method GET
 * url /tasks          
 */
$app->get('/dealership/inventory/add', 'authenticate', function() use ($app) {
                        
            require_once dirname(__FILE__) . '/../libs/BestDealer/Dealer.php';
            require_once dirname(__FILE__) . '/../libs/BestDealer/Edmunds.php';
            
            global $user_id;
            
            $response   = array();            
            $db         = new Dealer();
            
            $params["vinNumber"]    =  $app->request->params('vin');
            
            $db = new DbConnect();
            $conn = $db->connect();

            $sql = " SELECT 1 FROM `edmunds_search` where vinNumber = '" . $params["vinNumber"] ."'" ; 
            
                // insert query
            $stmt = $conn->prepare($sql);
                // $stmt->bindParam(":vin", $vinNumber);
            $result = $stmt->execute();            
                // Check for successful insertion
            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);             
            
            $rows = Edmunds::findCarByVIN($params["vinNumber"]);
            
            echoRespnse(200, $rows);
        }); 
        
/**
 * Listing all tasks of particual user
 * method GET
 * url /tasks          
 */
$app->get('/dealership/inventory/edit', 'authenticate', function() use ($app) {
                        
            require_once dirname(__FILE__) . '/../libs/BestDealer/Dealer.php';
            
            global $user_id;
            
            $response = array();            
            $db = new Dealer();
            $params = null;
            $params    = json_decode($app->request->getBody() , true );
            
            

            $results = $db->manageCompany($params);
            $response["error"] = true;
            $response["data"] = $results;           
            
            echoRespnse(200, $response);
        }); 
        
/**
 * Listing all tasks of particual user
 * method GET
 * url /tasks          
 */
$app->get('/dealership/inventory/delete', 'authenticate', function() use ($app) {
                        
            require_once dirname(__FILE__) . '/../libs/BestDealer/Dealer.php';
            
            global $user_id;
            
            $response = array();            
            $db = new Dealer();
            $params = null;
            $params    = json_decode($app->request->getBody() , true );
            
            

            $results = $db->manageCompany($params);
            $response["error"] = true;
            $response["data"] = $results;           
            
            echoRespnse(200, $response);
        });      
        
/**
 * Listing all tasks of particual user
 * method GET
 * url /tasks          
 */
$app->get('/dealership/inventory/offer', 'authenticate', function() use ($app) {
                        
            require_once dirname(__FILE__) . '/../libs/BestDealer/Dealer.php';
            
            global $user_id;
            
            $response = array();            
            $db = new Dealer();
            $params = null;
            $params    = json_decode($app->request->getBody() , true );
            
            

            $results = $db->manageCompany($params);
            $response["error"] = true;
            $response["data"] = $results;           
            
            echoRespnse(200, $response);
        });         

/**
 * Listing all tasks of particual user
 * method GET
 * url /tasks          
 */
$app->get('/dealership/manageworkers', 'authenticate', function() use ($app) {
                        

            verifyRequiredParams(array('email', 'password','cid'));

            // reading post params
            $params = null;
            $params    = json_decode($app->request->getBody() , true );

            // validating email address
            validateEmail($params["email"]);

            if(!isset($params["id"])){    

                $db = new DbHandler();
                $res = $db->createUser($params);

                if ($res == USER_CREATED_SUCCESSFULLY) {
                    $response["error"] = false;
                    $response["message"] = "You are successfully registered";
                } else if ($res == USER_CREATE_FAILED) {
                    $response["error"] = true;
                    $response["message"] = "Oops! An error occurred while registereing";
                } else if ($res == USER_ALREADY_EXISTED) {
                    $response["error"] = true;
                    $response["message"] = "Sorry, this email already existed";
                }

            } else {

                $db = new DbHandler();
                $res = $db->updateUser($params);

                if ($res == USER_CREATED_SUCCESSFULLY) {
                    $response["error"] = false;
                    $response["message"] = "You are successfully updated";
                } else if ($res == USER_CREATE_FAILED) {
                    $response["error"] = true;
                    $response["message"] = "Oops! An error occurred while updating";
                } else if ($res == USER_ALREADY_EXISTED) {
                    $response["error"] = true;
                    $response["message"] = "Sorry, this email already existed";
                }

            }

            // echo json response
            echoRespnse(201, $response);

        });


$app->get('/dealership/payment', 'authenticate', function() use ($app) {
                        
			verifyRequiredParams(array('email','plan','card_number', 'month','year','cvc_number'));
			
            require_once dirname(__FILE__) . '/../libs/Payment/init.php';
            
            global $user_id;
            			
            $response = array();            
            $payment = new Company();
            $params = null;
            $params    = json_decode($app->request->getBody() , true );			
			
			$response = $payment->processPayment($params);

            echoRespnse(200, $response);
 });		
		
 /*
 * ------------------------ INVENTORY Functions  ------------------------
 */


/**
 * Listing all tasks of particual user
 * method GET
 * url /tasks          
 */
$app->get('/dealership/inventory/favourites', 'authenticate', function() use ($app) {
                        
        require_once dirname(__FILE__) . '/../libs/BestDealer/Inventory.php';
            
            global $user_id;
            
        
            $response = array();            
            $db = new Inventory();
            $params = null;
            $params    = json_decode($app->request->getBody() , true );
            
            if(!isset($params["autoid"]))
                $autoid = null;
            else
                $autoid = $params["autoid"];
            
            $results = $db->getVehicles($user_id , $autoid , true );
            $response["error"] = true;
            $response["data"] = $results;           
            
            echoRespnse(200, $response);
        }); 

 
/**
 * Listing all tasks of particual user
 * method GET
 * url /tasks          
 */
$app->get('/dealership/inventory/listing(/:autoid)', 'authenticate', function($autoid = null) use ($app) {
						
	    require_once dirname(__FILE__) . '/../libs/BestDealer/Inventory.php';
            
	        global $user_id;
            
	    
            $response = array();			
            $db = new Inventory();
            $params = null;
            $params    = json_decode($app->request->getBody() , true );
			
			$results = $db->getVehicles($user_id , $autoid  );
			$response["error"] = true;
			$response["data"] = $results;			
			
            echoRespnse(200, $response);
        }); 
 

/**
 * Listing all tasks of particual user
 * method GET
 * url /tasks          
 */
$app->get('/dealership/feeds', 'authenticate', function($autoid = null) use ($app) {
						
	        require_once dirname(__FILE__) . '/../libs/BestDealer/Inventory.php';
            
	        global $user_id;
            
	    
			
            echoRespnse(200, $response);
        });


/**
 * Listing all tasks of particual user
 * method GET
 * url /tasks          
 */
$app->get('/dealership/offer/like/:offerid', 'authenticate', function($offerid) use ($app) {
						
	        require_once dirname(__FILE__) . '/../libs/BestDealer/Dealer.php';
            
	        global $user_id;
            
	    
			
			
            echoRespnse(200, $response);
        });


/**
 * Listing all tasks of particual user
 * method GET
 * url /tasks          
 */
$app->get('/dealership/tradeins', 'authenticate', function() use ($app) {
						
	        require_once dirname(__FILE__) . '/../libs/BestDealer/Inventory.php';
            
	        global $user_id;
 
            $inventory = new Inventory();
            $rows = $inventory->getTradeIns($user_id);
	    
            echoRespnse(200, $rows);
        });


/**
 * Listing all tasks of particual user
 * method GET
 * url /tasks          
 */
$app->get('/dealership/offerringOrseeking', 'authenticate', function() use ($app) {
						
	        require_once dirname(__FILE__) . '/../libs/BestDealer/Dealer.php';
            
	        global $user_id;
	        $dealer               = new Dealer();
            
            $response['error']    = false;
	        $option               = $app->request->params('option');
	        $params["title"]      = $app->request->params('title');
	        $params["autoid"]     = $app->request->params('autoid');
	        $params["price"]      = $app->request->params('price');
	        $params["shipping"]   = $app->request->params('shipping');
	        $params["createdBy"]  = $user_id;
	        $params["expires"]    = date("Y-m-d H:i:s" , strtotime("+".$app->request->params('expires')." day") ) ;
            $params["status"]     = 1;
            $params["created"]    = date("Y-m-d H:i:s"); 	        

            $results              = $dealer->offerringOrseeking($params ,  ($option == "offers") );
            
	        if($results == FAILED){
                $response['error'] = true;
                $response['message'] = "An error occurred. Please try again";
	        }
			
            echoRespnse(200, $response);
    });
        

/**
 * Listing all tasks of particual user
 * method GET
 * url /tasks          
 */
$app->get('/dealership/counter/', 'authenticate', function() use ($app) {
						
	        require_once dirname(__FILE__) . '/../libs/BestDealer/Inventory.php';
            
	        global $user_id;
	        $dealer               = new Dealer();
            
            $response['error']    = false;

	        $params["offerid"]    = $app->request->params('offerid');
	        $params["price"]      = $app->request->params('price');
	        $params["userid"]     = $user_id;

            $results              = $dealer->counterOffer($params);
	        
	        if($results == FAILED){
                $response['error'] = true;
                $response['message'] = "An error occurred. Please try again";
	        }
			
            echoRespnse(200, $response);
        });
        


/**
 * Listing all tasks of particual user
 * method GET
 * url /tasks          
 */
$app->get('/activity/feed(/:state)', 'authenticate', function($autoid = null ) use ($app) {
                        
            
            global $user_id;
            
        
            require_once dirname(__FILE__) . '/../include/DbConnect.php';
            // opening db connection
            $db = new DbConnect();
            $conn = $db->connect();

            $params    = $where = null;
            $params    = json_decode($app->request->getBody() , true );            
            $state     = $params["state"];

            if($state)
                $where = 'WHERE  state = '. $state;

            $sql = " SELECT * , concat(title , ' ' , price , ' ' , city ) as searchOn   FROM `activityfeed`  {$where}  ORDER BY `activityfeed`.`created` DESC" ; 
            // insert query
            $stmt = $conn->prepare($sql);
            // $stmt->bindParam(":vin", $vinNumber);
            $result = $stmt->execute();            
            // Check for successful insertion
            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $response = array();                                        
            $response["error"] = false;
            $response["data"]  = $rows;                       
            echoRespnse(200, $response);           

       
            
        });


/**
 * Listing all tasks of particual user
 * method GET
 * url /tasks          
 */
$app->get('/hasoffers(/:autoid)', 'authenticate', function($autoid = null ) use ($app) {
                        
            
            global $user_id;
            
        
            require_once dirname(__FILE__) . '/../include/DbConnect.php';
            // opening db connection
            $db = new DbConnect();
            $conn = $db->connect();

            $params = null;
            $params    = json_decode($app->request->getBody() , true );
            
            $where = 'WHERE  createdBy <> '. $user_id ;  

            if($offerid)
                $where .= " AND autoid = {$autoid}";

            $sql = " SELECT count(1) as count FROM `view_offers`  {$where} " ; 
            // insert query
            $stmt = $conn->prepare($sql);
            // $stmt->bindParam(":vin", $vinNumber);
            $result = $stmt->execute();            
            // Check for successful insertion
            $rs = $stmt->fetchAll(PDO::FETCH_ASSOC);   

            $response = array();                                        
            $response["results"] = is_array($rs);                      
            echoRespnse(200, $response);         
            
        }); 


/**
 * Listing all tasks of particual user
 * method GET
 * url /tasks          
 */
$app->get('/deals/', 'authenticate', function($autoid = null ) use ($app) {
                        
            
            global $user_id;
            
        
            require_once dirname(__FILE__) . '/../include/DbConnect.php';
            // opening db connection
            $db = new DbConnect();
            $conn = $db->connect();

            $params = null;
            $params    = json_decode($app->request->getBody() , true );
            
            $where = 'WHERE  createdBy <> '. $user_id ;  

            $sql = " SELECT id , year , make , model , trim , name , offerprice , mileage FROM `view_offers`  {$where} " ; 
            // insert query
            $stmt = $conn->prepare($sql);
            // $stmt->bindParam(":vin", $vinNumber);
            $result = $stmt->execute();            
            // Check for successful insertion
            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $response = array();                                        
            $response["error"] = false;
            $response["data"]  = $rows;                       
            echoRespnse(200, $response);         
            
        }); 

/**
 * Listing all tasks of particual user
 * method GET
 * url /tasks          
 */
$app->get('/offers/count', 'authenticate', function() use ($app) {
                        
            
            global $user_id;
            
        
            require_once dirname(__FILE__) . '/../include/DbConnect.php';
            // opening db connection
            $db = new DbConnect();
            $conn = $db->connect();

            $params = null;
            $params    = json_decode($app->request->getBody() , true );
            
            $where = 'WHERE  expires >= NOW() AND createdBy <> '. $user_id ;  

            $sql = " SELECT count(1)  as count FROM `offer`  {$where} " ; 
            // insert query
            $stmt = $conn->prepare($sql);
            // $stmt->bindParam(":vin", $vinNumber);
            $result = $stmt->execute();            
            // Check for successful insertion
            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $response = array();                                        
            $response["error"] = false;
            $response["data"]  = $rows[0];                       
            echoRespnse(200, $response);         
            
        }); 

/**
 * Listing all tasks of particual user
 * method GET
 * url /tasks          
 */
$app->get('/offers(/:id)', 'authenticate', function($id = null ) use ($app) {
                        
            
            global $user_id;
            
        
            require_once dirname(__FILE__) . '/../include/DbConnect.php';
            // opening db connection
            $db = new DbConnect();
            $conn = $db->connect();

            $params = null;
            $params    = json_decode($app->request->getBody() , true );
            
            $where = 'WHERE  createdBy <> '. $user_id ;  

            if($id)
                $where .= " AND id = {$id}";

            $sql = " SELECT id , searchOn ,year , make , model , trim , name , offerprice , mileage FROM `view_offers`  {$where} " ; 
            // insert query
            $stmt = $conn->prepare($sql);
            // $stmt->bindParam(":vin", $vinNumber);
            $result = $stmt->execute();            
            // Check for successful insertion
            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $response = array();                                        
            $response["error"] = false;
            $response["data"]  = $rows;                       
            echoRespnse(200, $response);         
            
        }); 

$app->get('/inventory/offers(/:autoid)', 'authenticate', function($autoid = null ) use ($app) {
                        
            
            global $user_id;
            
        
            require_once dirname(__FILE__) . '/../include/DbConnect.php';
            // opening db connection
            $db = new DbConnect();
            $conn = $db->connect();

            $params = null;
            $params    = json_decode($app->request->getBody() , true );
            
            $where = 'WHERE  createdBy <> '. $user_id ;  

            if($offerid)
                $where .= " AND autoid = {$autoid}";

            $sql = " SELECT id , year , make , model , trim , name , offerprice , mileage FROM `view_offers`  {$where} " ; 
            // insert query
            $stmt = $conn->prepare($sql);
            // $stmt->bindParam(":vin", $vinNumber);
            $result = $stmt->execute();            
            // Check for successful insertion
            $rs = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $rows = $rs[0];
               

            $response = array();                                        
            $response["error"] = false;
            $response["data"]  = $rows;                       
            echoRespnse(200, $response);         
            
        }); 


/**
 * Listing all tasks of particual user
 * method GET
 * url /tasks          
 */
$app->get('/offer/create', 'authenticate', function() use ($app) {
                        
            
            global $user_id;
            
        
            require_once dirname(__FILE__) . '/../include/DbConnect.php';
            // opening db connection
            $db = new DbConnect();
            $conn = $db->connect();

            $params = null;
            $params    = json_decode($app->request->getBody() , true );

            verifyRequiredParams(array( 'autoid', 'price','expires'));

            $params["createdBy"] = $user_id;
            
            $fields = implode("," , array_keys($params));
            $values = implode( "," , array_map(function($value) { return ':' . $value; }, array_keys($params) ) );

            $sql = "INSERT INTO offer (".$fields.") VALUES (".$values.")";
            // insert query
           
            $stmt = $conn->prepare($sql);
            foreach($params as $key => $value ){
                $stmt->bindValue($key, $value); //$stmt->bindParam(":".$key , $value );
            }            

            if($stmt->execute()){
                $response["error"] = false;   
            }else{
                $response["error"] = true;              
            } 
                                                            
            echoRespnse(200, $response);         
            
        }); 

/**
 * Listing all tasks of particual user
 * method GET
 * url /tasks          
 */
$app->get('/offer/submit', 'authenticate', function() use ($app) {
                        

            global $user_id;
            
        
            require_once dirname(__FILE__) . '/../include/DbConnect.php';
            // opening db connection
            $db = new DbConnect();
            $conn = $db->connect();

            $params = null;
            $params    = json_decode($app->request->getBody() , true );

            verifyRequiredParams(array( 'autoid', 'price'));

            if(isset($params["expires"])){
                $params["expires"] = null;
            }    

            $params["createdBy"] = $user_id;
            
            $fields = implode("," , array_keys($params));
            $values = implode( "," , array_map(function($value) { return ':' . $value; }, array_keys($params) ) );

            $sql = "INSERT INTO offer (".$fields.") VALUES (".$values.")";
            // insert query
           
            try {
                    $stmt = $conn->prepare($sql);
                    
                    foreach($params as $key => $value ){
                        $stmt->bindValue($key, $value); //$stmt->bindParam(":".$key , $value );
                    }
                    
                    if($stmt->execute()){
                        $response["error"] = false;
                        $response["data"]  = $rows;    
                    }else{
                        $response["error"] = true;
                        $response["data"]  = "Offer not submitted";                
                    } 
                
                // $sql = "update offer (".$fields.") VALUES (".$values.")";                            

            } catch (Exception $e) {
                die("Oh noes! There's an error in the query!");
            }

            $response = array();                                                             
            echoRespnse(200, $response);        
            
        }); 



/**
 * Listing all tasks of particual user
 * method GET
 * url /tasks          
 */
$app->get('/offer/accept', 'authenticate', function() use ($app) {
                                    
            global $user_id;
            
        
            require_once dirname(__FILE__) . '/../include/DbConnect.php';
            // opening db connection
            $db = new DbConnect();
            $conn = $db->connect();

            $params = null;
            $params    = json_decode($app->request->getBody() , true );

            verifyRequiredParams(array( 'offerid','autoid','offererid','price')); 

            $offerid =  $params["offerid"];   
            $price   =  $params["price"];  
            $autoid   =  $params["autoid"];  
            
            $sql  = " UPDATE offer SET status = 1 WHERE offerid = {$offerid} ";
            $sql1 = " UPDATE inventory SET offerprice = '{$price}', solddate = NOW() 
                      WHERE autoid = {$autoid} AND createdBy = {$offerid} ";
            $sql2 = " INSERT inventory (starred,styleid,vin,year,make,model,body,trim,name,mileage,purchaseprice,purchasedate,createdBy) SELECT 0 ,styleid,vin,year,make,model,body,trim,name,mileage,{$price},purchasedate,{$offerid} FROM  inventory WHERE autoid = {$autoid} AND createdBy = {$offerid}  ";
            // insert query
           
            try {
                    $stmt = $conn->prepare($sql);
                    
                    foreach($params as $key => $value ){
                        $stmt->bindValue($key, $value); //$stmt->bindParam(":".$key , $value );
                    }
                    
                    if($stmt->execute()){
                       
                       $stmt = $conn->prepare($sql1); 
                       $stmt->execute();
                       $stmt = $conn->prepare($sql1); 
                       $stmt->execute();


                    }else{
                        $response["error"] = true;
                        $response["data"]  = "Offer not submitted";                
                    } 
                
                // $sql = "update offer (".$fields.") VALUES (".$values.")";                            

            } catch (Exception $e) {
                die("Oh noes! There's an error in the query!");
            }

            $response = array();                                                             
            echoRespnse(200, $response);          
            
        }); 

 
/**
	* Listing all tasks of particual user
 * method GET
 * url /tasks  
 *          SOLD/UNSOLD
 *          STARRED/UNSTARRED 
 */
$app->get('/inventory/status', 'authenticate', function() use ($app) {
						
						
			verifyRequiredParams(array('soldprice', 'autoid'));			
						
			require_once dirname(__FILE__) . '/../libs/BestDealer/Inventory.php';
            
			global $user_id;
			
            $response = array();			
            $db = new Inventory();
            $params = null;
            $params    = json_decode($app->request->getBody() , true );
			
			$results = $db->vehicleStatus( $params , $user_id );
            
			if ($results) {
				$response["error"] = false;
			} else {
				// unknown error occurred
				$response['error'] = true;
				$response['message'] = "An error occurred. Please try again";
			}			
			
            echoRespnse(200, $response);
        }); 
 
 
 /**
 * Listing all tasks of particual user
 * method GET
 * url /tasks          
 */
$app->get('/inventory/manage', 'authenticate', function() use ($app) {
			
			
			
            $params = null;
            $params    = json_decode($app->request->getBody() , true );			
			
			if(isset($params["vin"])){
				
				verifyRequiredParams(array('vin','action'));
				
                if(!validateVinNumber($params["vin"])){
                    $response["error"] = FAILED;
                    $response["message"] = "Not a valid VIN Number";                      
                    echoRespnse(200, $response);
					return ;
                    
                }
                 
            }
			else	
				verifyRequiredParams(array('autoid','action'));

			global $user_id;
            $response = array();			
           
			
			$params["createdBy"] = $user_id;			
			
			$action = $params["action"];
			unset($params["action"]);
			
            require_once dirname(__FILE__) . '/../libs/BestDealer/Inventory.php';
			 $db = new Inventory();
			
            switch ($action) {
                case "Add":
                        $response = $db->addVehicle($params);
                        break;
                case "Edit":
                        $response = $db->editVehicle($params);
                        break;
                case "Delete":
                        $response = $db->deleteVehicle($params);
                        break;
            }           

            echoRespnse(200, $response);
        }); 


 /**
 * Prep a vechile to be sold
 * method GET
 * url /tasks          
 */
$app->get('/inventory/prep/$autoid', 'authenticate', function($autoid) use ($app) {
            
            verifyRequiredParams(array('autoid'));  

                            
            $sql = " SELECT mmid as id , searchOn as name  FROM `view_make_model_mmid` WHERE {$search} limit 50  " ; 
            // insert query
            $stmt = $conn->prepare($sql);
            // $stmt->bindParam(":vin", $vinNumber);
            $result = $stmt->execute();            
            // Check for successful insertion
            $rows["items"] = $stmt->fetchAll(PDO::FETCH_ASSOC);   


        });
				
 /**
 * Prep a vechile to be sold
 * method GET
 * url /tasks          
 */
$app->get('/inventory/prep', 'authenticate', function() use ($app) {
			
			verifyRequiredParams(array('autoid'));			
            
			global $user_id;
            $response = array();			
           
            $params = null;
            $params    = json_decode($app->request->getBody() , true );			
			$params["createdBy"] = $user_id;			
			
            $db = new DbConnect();
            $conn = $db->connect();            
			
            $params["status"]    = 1;
            $params["created"]   = date("Y-m-d H:i:s");                 
            // insert query
            
            $sql = "INSERT INTO inventory_expenses (".implode("," , array_keys($params)).") VALUES (".implode( "," , array_map(function($value) { return ':' . $value; }, array_keys($params) ) ).")";
            // insert query
            $stmt = $conn->prepare($sql);
            
            foreach($params as $key => $value ){
                $stmt->bindValue($key, $value); //$stmt->bindParam(":".$key , $value );
            }
            
            $result = $stmt->execute();     
            
            if ($result) {
                $response["error"] = SUCCESSFULLY;
                $response["message"] = "Added ";                

            } else {
                $response["error"] = FAILED;
                $response["message"] = "Failed";                

            }


            echoRespnse(200, $response);
        }); 
 
/**
	* Listing all tasks of particual user
 * method GET
 * url /tasks          
 */
$app->get('/vechileinfo', 'authenticate', function() use ($app) {
						
			require_once dirname(__FILE__) . '/../libs/BestDealer/Inventory.php';
			
			
            
			global $user_id;
			
            $response = array();			
            $db = new Inventory();
            $params = null;
            $params    = json_decode($app->request->getBody() , true );

			verifyRequiredParams(array('styleID'));
			
			$results = $db->getVehicleByStyleID($params["styleID"]);
			$response["error"] = true;
			$response["data"] = $results;			
			
            echoRespnse(200, $response);
        });  
 

$app->get('/add/image', 'authenticate', function() use ($app) {
			
			verifyRequiredParams(array('styleID'));
			
			$response["error"] = true;
			$response["data"]  = null;	
			
			 if (!isset($_FILES['uploads'])) {
					$response["data"]  = "No files uploaded!!";
					echoRespnse(200, $response);
				}
				$imgs = array();

				$files = $_FILES['uploads'];
				$cnt = count($files['name']);

				for($i = 0 ; $i < $cnt ; $i++) {
					if ($files['error'][$i] === 0) {
						$name = uniqid('img-'.date('Ymd').'-');
						if (move_uploaded_file($files['tmp_name'][$i], 'uploads/'. $cid .'/' . $name) === true) {
							$imgs[] = array('url' => 'uploads/'. $cid .'/' . $name , 'name' => $files['name'][$i]);
						}

					}
				}

				$imageCount = count($imgs);

				if ($imageCount == 0) {
				   $response["data"]  = 'No files uploaded!!  <p><a href="/">Try again</a>';
				   echoRespnse(200, $response);
				}

				$response["error"] = false;
			    $response["data"]  = $imgs;	
				echoRespnse(200, $response);
        });
		
 


        
 /*
 *                      VECHILE RISKER  STARTS 
 *
 *
 */

$app->get('/risk/byVinNumberZip', 'authenticate', function() use ($app) {
			
				verifyRequiredParams(array('vin'));
			
				require_once dirname(__FILE__) . '/../libs/BestDealer/Dealer.php';
				            
				global $user_id;
				
				$response = array();			

				$params = null;
				$params    = json_decode($app->request->getBody() , true );	

				$response 				 = Dealer::GenerateVehicleData($params);							
				echoRespnse(200, $response);
        });
		
 
$app->get('/risk/notVinNumber', 'authenticate', function() use ($app) {
			
			verifyRequiredParams(array('model','make','year','mileage'));
			
				require_once dirname(__FILE__) . '/../libs/BestDealer/Dealer.php';
				            
				global $user_id;
				
				$response = array();			

				$params = null;
				$params    = json_decode($app->request->getBody() , true );	

				$response 				 = Dealer::GenerateVehicleData($params);							
				echoRespnse(200, $response);
        });

        
 /*
 *                      VECHILE RISKER ENDS
 *
 *  http://localhost.rest/search/vechile/
 */


    $app->get('/search/vehicle', 'authenticate', function() use ($app) {
                        
                require_once dirname(__FILE__) . '/../include/DbConnect.php';
                // opening db connection
                
                $db = new DbConnect();
                $conn = $db->connect();

                $search = '';
                $params = null;
                $params    = json_decode($app->request->getBody() , true );                
                $keywords = explode(" ", $params['keywords']);


                if(is_array($keywords) && count($keywords) > 1 ){
                    
                    $sql = array();
                    foreach($keywords as $word){
                        $sql[] = " searchOn LIKE '%{$word}%' ";
                    }

                    $search = implode(" AND ", $sql);

                }else{
                     $search  = " searchOn LIKE '%{$keywords[0]}%' ";
                }    
                            
                $sql = " SELECT mmid as id , searchOn as name  FROM `view_make_model_mmid` WHERE {$search} limit 50  " ; 
                // insert query
                $stmt = $conn->prepare($sql);
                // $stmt->bindParam(":vin", $vinNumber);
                $result = $stmt->execute();            
                // Check for successful insertion
                $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);              


                $response = array();                                        
                //$response["error"] = false;
                $response  = $rows;                       
                echoRespnse(200, $response);
        });

		
 /*
 *                      REPORT SECTION STARTS 
 *
 *
 */
    $app->get('/dashboard', 'authenticate', function() use ($app) {
                        

                global $user_id;
                

                require_once dirname(__FILE__) . '/../include/DbConnect.php';
                // opening db connection
                $db = new DbConnect();
                $conn = $db->connect();
                            
                $sql = " SELECT * FROM `view_events` ORDER BY `view_events`.`auctionDate` DESC limit 3  " ; 
                // insert query
                $stmt = $conn->prepare($sql);
                // $stmt->bindParam(":vin", $vinNumber);
                $result = $stmt->execute();            
                // Check for successful insertion
                $rows["events"] = $stmt->fetchAll(PDO::FETCH_ASSOC);
                $where = 'WHERE  createdBy <> '. $user_id ;

                $sql = " SELECT * FROM `view_offers`  {$where} ORDER BY `view_offers`.`expires`  ASC  limit 3  " ; 
                // insert query
                $stmt = $conn->prepare($sql);
                // $stmt->bindParam(":vin", $vinNumber);
                $result = $stmt->execute();            
                // Check for successful insertion
                $rows["listing"] = $stmt->fetchAll(PDO::FETCH_ASSOC);                

                $response = array();                                        
                $response["error"] = false;
                $response["data"]  = $rows;                       
                echoRespnse(200, $response);
        });





   $app->get('/reports/dashboard', 'authenticate', function() use ($app) {
						
				require_once dirname(__FILE__) . '/../libs/BestDealer/Reports.php';
				            
				global $user_id;
				
				$reports = new Reports();
				$response = array();                						
                $response["error"] = false;
                $response["data"]  = $reports->getFinanceDashboard($user_id); 						
				echoRespnse(200, $response);
        });
		


        
 /*
 *                      REPORT SECTION ENDS 
 *
 *
 */
 


function validateVinNumber($vin) {

    $vin = strtolower($vin);
    if (!preg_match('/^[^\Wioq]{17}$/', $vin)) { 
        return false; 
    }

    $weights = array(8, 7, 6, 5, 4, 3, 2, 10, 0, 9, 8, 7, 6, 5, 4, 3, 2);

    $transliterations = array(
        "a" => 1, "b" => 2, "c" => 3, "d" => 4,
        "e" => 5, "f" => 6, "g" => 7, "h" => 8,
        "j" => 1, "k" => 2, "l" => 3, "m" => 4,
        "n" => 5, "p" => 7, "r" => 9, "s" => 2,
        "t" => 3, "u" => 4, "v" => 5, "w" => 6,
        "x" => 7, "y" => 8, "z" => 9
    );

    $sum = 0;

    for($i = 0 ; $i < strlen($vin) ; $i++ ) { // loop through characters of VIN
        // add transliterations * weight of their positions to get the sum
        if(!is_numeric($vin{$i})) {
            $sum += $transliterations[$vin{$i}] * $weights[$i];
        } else {
            $sum += $vin{$i} * $weights[$i];
        }
    }

    // find checkdigit by taking the mod of the sum

    $checkdigit = $sum % 11;

    if($checkdigit == 10) { // checkdigit of 10 is represented by "X"
        $checkdigit = "x";
    }

    return ($checkdigit == $vin{8});
}
 

/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
	 $app = \Slim\Slim::getInstance();
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
	

	if(isset($_REQUEST) && empty($_REQUEST)){
		$json = $app->request->getBody();
		$request_params = json_decode($json, true);
	}

	
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }

    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(400, $response);
        $app->stop();
    }
}

/**
 * Validating email address
 */
function validateEmail($email) {
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
	
        $response["error"] = true;
        $response["message"] = 'Email address ('.$email.') is not valid';
        echoRespnse(400, $response);
        $app->stop();
    }
}

function uploadFile () {
	
	
	
	$response["error"] = true;
	$response["message"] = 'No files uploaded';	
	
    if (!isset($_FILES['uploads'])) {
        echoRespnse(400, $response);
        $app->stop();
    }
    $imgs = array();

    $files = $_FILES['uploads'];
    $cnt = count($files['name']);

    for($i = 0 ; $i < $cnt ; $i++) {
        if ($files['error'][$i] === 0) {
            $name = uniqid('img-'.date('Ymd').'-');
            if (move_uploaded_file($files['tmp_name'][$i], '/uploads/'.$cid.'/' . $name ) === true) {
                $imgs[] = array('url' => '/uploads/'.$cid.'/' . $name, 'name' => $files['name'][$i]);
            }

        }
    }

    $imageCount = count($imgs);

    if ($imageCount == 0) {
        echoRespnse(400, $response);
        $app->stop();
    }

    $plural = ($imageCount == 1) ? '' : 's';

    foreach($imgs as $img) {
        printf('%s <img src="%s" width="50" height="50" /><br/>', $img['name'], $img['url']);
    }
	
	echoRespnse(400, $response);
	$app->stop();	
	
}


/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);

    // setting response content type to json
    $app->contentType('application/json');

    if( isset($response["data"]) && empty($response["data"]))
        $response["data"] = null;    

    echo json_encode($response);
}

function  getIPAddress() {

	if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
		$ip = $_SERVER['HTTP_CLIENT_IP'];
	} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
		$ip = $_SERVER['REMOTE_ADDR'];
	}
	
		return $ip;
}

$app->run();
?>
