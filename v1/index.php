<?php

require_once '../include/DbHandler.php';
require_once '../include/PassHash.php';
require '../libs/Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();
$app->contentType('application/json');

// User id from db - Global Variable
$user_id = NULL;
$cid     = NULL;
$access  = NULL;

/**
 * Adding Middle Layer to authenticate every request
 * Checking if the request has valid api key in the 'Authorization' header
 */
function authenticate(\Slim\Route $route) {
    // Getting request headers
    $headers  = apache_request_headers();
    $response = array();
    $app      = \Slim\Slim::getInstance();
    

    // Verifying Authorization Header
    if (isset($headers['authorization'])) {
        $db = new DbHandler();

        // get the api key
        $api_key = $headers['authorization'];

        // validating api key
        if ( $db->isValidApiKey($api_key) === false ) {
            // api key is not present in users table
            $response["error"] = true;
            $response["message"] = "Access Denied. Invalid Api key";
            echoRespnse(401, $response);
            $app->stop();
        } else {
            global $user_id;
            // get user primary key id
            $user    = $db->getUserId($api_key);
			$user_id = $user["userid"];

        }
    } else {
        // api key is missing in header
        $response["error"] = true;
        $response["message"] = "Api key is misssing" . implode("," , $headers );
        echoRespnse(400, $response);
        $app->stop();
    }
}

/**
 * ----------- METHODS WITHOUT AUTHENTICATION ---------------------------------
 */

/**
 * ----------- METHODS WITHOUT AUTHENTICATION ---------------------------------
 */
 
 
 /**
 * User Registration
 * url - /register
 * method - POST
 * params - name, email, password
 */

 // GET route
$app->get(
    '/',
    function () {
        $template = <<<EOT
            Welcome to BESTDEALER API
EOT;
        echoRespnse(201, $template); 
        //echo $template;
    }
);


/**
 * Listing all tasks of particual user
 * method GET
 * url /tasks          
 */
$app->post('/dealership/register',  function() use ($app) {
                        
            // check for required params
            verifyRequiredParams(array( 'email', 'password'));

            $response  = array();
            $params    =  $app->request->params();
            // validating email address
            validateEmail($params["email"]);

            $db = new DbHandler();
            $db->beginTransaction();
            $res = $db->createUser($params);

            if ($res == USER_CREATED_SUCCESSFULLY) {
                
                require_once dirname(__FILE__) . '/../libs/stripe/init.php';    
                \Stripe\Stripe::setApiKey('sk_test_T8dTq7IAFkkWt6tscB3dcEKE');
                $result = \Stripe\Customer::create(array(
                    "email" => $params["email"],
                    "description" => "Mobile Customer",
                    //"coupon" => (isset($params["coupon"]))?$params["coupon"]:"none",
                    // "trial_end" =>  strtotime("+1 month") 
                    // obtained with Stripe.js
                )); 
                $array = array( "email" => $params["email"] , "customerid" => $result["id"]   );
                $db->updateUser($array);    
                $db->commit();        
                $response["error"] = false;
                $response["message"] = "You are successfully registered";
            } else if ($res == USER_CREATE_FAILED) {
                $db->rollback();
                $response["error"] = true;
                $response["message"] = "Oops! An error occurred while registereing";
            } else if ($res == USER_ALREADY_EXISTED) {
                $db->rollback();
                $response["error"] = true;
                $response["message"] = "Sorry, this email already existed";
            }
            // echo json response
            echoRespnse(200, $response);
        });
        

$app->post('/dealership/login', function() use ($app) {
                        
            // check for required params
			verifyRequiredParams(array('email', 'password'));
			
            $response       = array();
            $email          =  $app->request->params('email');
            $password       =  $app->request->params('password');            

            $db             = new DbHandler();
            // check for correct email and password
			$status         = $db->checkLogin($email, $password);
			
            if ($status) {
                // get the user by email
                $user = $db->getUserByEmail($email);

                if ($user != NULL) {
                    $response["error"] = false;
                    $response['email'] = $user['email'];
                    $response['apiKey'] = $user['api_key'];
                    $response['created'] = $user['created'];
                    $db->lastVisited($user["userid"]);

                } else {
                    // unknown error occurred
                    $response['error'] = true;
                    $response['message'] = "An error occurred. Please try again";
                }
            } else {
                // user credentials are wrong
                $response['error'] = true;
                if(is_null($status))
                    $response['message'] = 'Account inactive.Please contact customer care.';
                else
                    $response['message'] = 'Login failed. Incorrect credentials';
            }

            echoRespnse(200, $response);
        }); 


$app->post('/dealership/forgot', function() use ($app) {
                        
            // check for required params
			verifyRequiredParams(array('email'));
			
            $response  = array();
            $email =  $app->request->params('email');
            
            validateEmail($email);
            
            $db = new DbHandler();
            // check for correct email and password
            $response = $db->resetUser($email);

            echoRespnse(200, $response);
        }); 

$app->post('/dealership/deactivate', 'authenticate' , function() use ($app) {
                        
            global $user_id;
            $db         = new DbHandler();
            $response   = $db->deactivateUser($user_id);
            echoRespnse(200, $response);
        }); 

$app->post('/dealership/payment', 'authenticate' , function() use ($app) {
                        
            global $user_id;
            
            $params     =  $app->request->params();
            $db         = new DbHandler();
            $response   = $db->addPayment($params);
            echoRespnse(200, $response);
            
        }); 
        
        
$app->post('/dealership/inventory/add', 'authenticate' , function() use ($app) {
                        
            require_once dirname(__FILE__) . '/../libs/BestDealer/Edmunds.php';
            require_once dirname(__FILE__) . '/../libs/BestDealer/Inventory.php';
            
            global $user_id;
            
		
            $inventory = new Inventory();
            $params    =  $app->request->params();
            $vin       = $params['vinNumber'];
            
            if(isset($vin)){
                $rows = Edmunds::findCarByVIN($vin);
                
                if(isset($rows['years'][0]['styles'][0]['id']))
                    $params['styleid'] = $rows['years'][0]['styles'][0]['id'];
                else {
                    $response["error"] = FAILED;
			        $response["message"] = "Vehicle couldnt be added please check VIN number";
                }    
                
                unset($params['vinNumber']);
            }
            
            $params['createdBy'] = $user_id;
            

            if(!empty($params['styleid'])){
                $response = $inventory->addVehicle($params);
            }
            
            echoRespnse(200, $response);
            
        }); 
        
$app->post('/dealership/inventory/edit/$autoid', 'authenticate' , function($autoid) use ($app) {
                        
            require_once dirname(__FILE__) . '/../libs/BestDealer/Inventory.php';
            
            global $user_id;
            $inventory  = new Inventory();
            $params     =  $app->request->params();
            $rows       = $inventory->editVehicle($params , $autoid );
            echoRespnse(200, $rows);
        });         
        
$app->post('/dealership/inventory/delete/:autoid', 'authenticate' , function($autoid) use ($app) {
                        
            global $user_id;
            $inventory  = new Inventory();
            $rows       = $inventory->deleteVehicle($autoid);
            echoRespnse(200, $response);
        });         

$app->post('/dealership/inventory/prep/:autoid', 'authenticate' , function($autoid) use ($app) {
                        
            global $user_id;
            $params     =  $app->request->params();
            $rows       = $inventory->prepVehicle($params , $autoid );
            echoRespnse(200, $response);
        }); 

$app->get('/dealership/trending', 'authenticate' , function() use ($app) {
                        
            global $user_id;

            echoRespnse(200, $response);
        });         
        
$app->get('/dealership/offers(/:autoid)', 'authenticate' , function($auto=null) use ($app) {
                        
            require_once dirname(__FILE__) . '/../libs/BestDealer/Inventory.php';
            
            global $user_id;
            $inventory  = new Inventory();
            $response = $inventory->getOffers($user_id);
            echoRespnse(200, $response);
        });  
        
        
$app->post('/dealership/offers/counter/:autoid', 'authenticate' , function($autoid) use ($app) {
                        
            require_once dirname(__FILE__) . '/../libs/BestDealer/Inventory.php';
            
            global $user_id;
            $inventory  = new Inventory();
            $params     =  $app->request->params();
            $response   = $inventory->submitCounterOffer($params , $autoid);
            echoRespnse(200, $response);
            
        });         


$app->get('/dealership/searchOn', 'authenticate' , function() use ($app) {
                        
            require_once dirname(__FILE__) . '/../libs/BestDealer/Dealer.php';                
            // check for required params
		    global $user_id;
			
            $response  = array();
            $inventory  = new Dealer();
            $response   = $inventory->getReports();
            echoRespnse(200, $response);
        });

        
$app->get('/dealership/seeking', 'authenticate' , function() use ($app) {
                        
            require_once dirname(__FILE__) . '/../libs/BestDealer/Dealer.php';                
            // check for required params
		    global $user_id;
			
            $response  = array();
            $inventory  = new Dealer();
            $response   = $inventory->getReports();
            echoRespnse(200, $response);
        });  
        

$app->get('/dealership/reports', 'authenticate' , function() use ($app) {
                        
            require_once dirname(__FILE__) . '/../libs/BestDealer/Reports.php';                
            // check for required params
		    global $user_id;
			
            $response  = array();
            $inventory  = new Reports();
            $response   = $inventory->getReports();
            echoRespnse(200, $response);
        });         



function validateVinNumber($vin) {

    $vin = strtolower($vin);
    if (!preg_match('/^[^\Wioq]{17}$/', $vin)) { 
        return false; 
    }

    $weights = array(8, 7, 6, 5, 4, 3, 2, 10, 0, 9, 8, 7, 6, 5, 4, 3, 2);

    $transliterations = array(
        "a" => 1, "b" => 2, "c" => 3, "d" => 4,
        "e" => 5, "f" => 6, "g" => 7, "h" => 8,
        "j" => 1, "k" => 2, "l" => 3, "m" => 4,
        "n" => 5, "p" => 7, "r" => 9, "s" => 2,
        "t" => 3, "u" => 4, "v" => 5, "w" => 6,
        "x" => 7, "y" => 8, "z" => 9
    );

    $sum = 0;

    for($i = 0 ; $i < strlen($vin) ; $i++ ) { // loop through characters of VIN
        // add transliterations * weight of their positions to get the sum
        if(!is_numeric($vin{$i})) {
            $sum += $transliterations[$vin{$i}] * $weights[$i];
        } else {
            $sum += $vin{$i} * $weights[$i];
        }
    }

    // find checkdigit by taking the mod of the sum

    $checkdigit = $sum % 11;

    if($checkdigit == 10) { // checkdigit of 10 is represented by "X"
        $checkdigit = "x";
    }

    return ($checkdigit == $vin{8});
}
 

/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
	 $app = \Slim\Slim::getInstance();
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
	

	if(isset($_REQUEST) && empty($_REQUEST)){
		$json = $app->request->getBody();
		$request_params = json_decode($json, true);
	}

	
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }

    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(400, $response);
        $app->stop();
    }
}

/**
 * Validating email address
 */
function validateEmail($email) {
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
	
        $response["error"] = true;
        $response["message"] = 'Email address ('.$email.') is not valid';
        echoRespnse(400, $response);
        $app->stop();
    }
}

function uploadFile () {
	
	
	
	$response["error"] = true;
	$response["message"] = 'No files uploaded';	
	
    if (!isset($_FILES['uploads'])) {
        echoRespnse(400, $response);
        $app->stop();
    }
    $imgs = array();

    $files = $_FILES['uploads'];
    $cnt = count($files['name']);

    for($i = 0 ; $i < $cnt ; $i++) {
        if ($files['error'][$i] === 0) {
            $name = uniqid('img-'.date('Ymd').'-');
            if (move_uploaded_file($files['tmp_name'][$i], '/uploads/'.$cid.'/' . $name ) === true) {
                $imgs[] = array('url' => '/uploads/'.$cid.'/' . $name, 'name' => $files['name'][$i]);
            }

        }
    }

    $imageCount = count($imgs);

    if ($imageCount == 0) {
        echoRespnse(400, $response);
        $app->stop();
    }

    $plural = ($imageCount == 1) ? '' : 's';

    foreach($imgs as $img) {
        printf('%s <img src="%s" width="50" height="50" /><br/>', $img['name'], $img['url']);
    }
	
	echoRespnse(400, $response);
	$app->stop();	
	
}


/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);

    // setting response content type to json
    $app->contentType('application/json');

    if( isset($response["data"]) && empty($response["data"]))
        $response["data"] = null;    

    echo json_encode($response);
}

function  getIPAddress() {

	if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
		$ip = $_SERVER['HTTP_CLIENT_IP'];
	} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
		$ip = $_SERVER['REMOTE_ADDR'];
	}
	
		return $ip;
}




$app->run();
?>
